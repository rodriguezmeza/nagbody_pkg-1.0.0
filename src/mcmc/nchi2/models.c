/*==============================================================================
	MODULE: models.c			[nchi2]
	Written by: M.A. Rodriguez-Meza
	Starting date: January 2018
	Purpose: Rutines to create several types of models
	Language: C
	Use: 'testmodel();'
	Routines and functions:
	Modules, routines and external headers:
	Coments and notes:
	Info: M.A. Rodriguez-Meza
		Depto. de Fisica, ININ
		Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
		e-mail: marioalberto.rodriguez@inin.gob.mx
		http://www.astro.inin.mx/mar

	Mayor revisions: January 22, 2018
	Copyright: (c) 2005-2018 Mar.  All Rights Reserved
================================================================================
	Legal matters:
	The author does not warrant that the program and routines it contains
	listed below are free from error or suitable for particular applications,
	and he disclaims all liability from any consequences arising from their	use.
==============================================================================*/

#define global                                  // check the behaviour of this
#include "globaldefs.h"

local void model_string_to_int(string, int *);

// Models
local void Model_RCBECHO(void);
local void Model_RCPISO(void);


#define RCBECHO                     0
#define RCPISO                      1

void set_model(void)
{
	int model_int;

	model_string_to_int(cmd.model, &model_int);
	switch (model_int){

	case RCBECHO: Model_RCBECHO(); break;
	case RCPISO: Model_RCPISO(); break;

	default: error("\nUnknown model type %s\n\n",cmd.model);
	}
    
    fprintf(stdout,"\nNumber of parameters to fit: %d\n",nparams);
}

local void model_string_to_int(string model_str,int *model_int)
{
	*model_int = -1;
	if (strcmp(model_str,"RCBECHO") == 0)			*model_int=RCBECHO;
	if (strcmp(model_str,"RCPISO") == 0)            *model_int=RCPISO;
}

#undef RCBECHO
#undef RCPISO


// MODELS -------------------

// ==========================================
// Begin: RC BEC-HO Model


// params[1] === rhos   :: range: 0 < p1 < 10000
// params[2] === rs     :: range: 0 < p2 < 10

local void Model_RCBECHO(void)
{
    strcpy(gd.model_comment, "RCBECHO Model");
    
// Starting proccess...

    nparams = 2;
    params = dvector(1,nparams);
    rparams = dmatrix(1,nparams,1,2);
    rparams[1][1] = 0.0001;
    rparams[1][2] = 10000.0;
    rparams[2][1] = 0.0001;
    rparams[2][2] = 10.0;
}

global real IrhoDM(real x, real params[])
{
    real Irhotmp;
    
    Irhotmp = (1.0/4.0)
    *(
      -2.0*rexp(-rsqr(x)/rsqr(params[2]))*(x/params[2])
      + rsqrt(PI)*erff(x/params[2])
      );
    
    return (Irhotmp);
}

global real massDM(real x, real params[])
{
    real masstmp;
    
    masstmp = params[1]*rpow(params[2],3.0)*(1.0/4.0)
            *(
              -2.0*rexp(-rsqr(x)/rsqr(params[2]))*(x/params[2])
              + rsqrt(PI)*erff(x/params[2])
              );

    return (masstmp);
}

global real vcDM(real x, real params[])
{
    real vcDMtmp;
    
    vcDMtmp = rsqrt(massDM(x,params)/x);
    
    return vcDMtmp;
}

global real vcTotalBECHO(real x, real params[])
{
    real vcTtmp;
    
    vcTtmp = rsqrt( rsqr(vcDM(x,params)) );
    
    return vcTtmp;
}

global void test_Chi2_BECHO(void)
{
    real Chi2tmp;
    real Chi2red;
    
    params[1] = 4899.44;
    params[2] = 3.65868;
// Chi2     = 1.72175                       // For galaxy F563-V2_rotmod.txt
// Chi2red  = 0.215219

    Chi2tmp = Chi2(vcTotalBECHO,
                   params,
                   xObs(pObs), yObs(pObs), sigmaObs(pObs), nObs(pObs));
    Chi2red = Chi2tmp/(nObs(pObs)-nparams);
    fprintf(stdout,"Chi2: %g %g %g %g\n",params[1],params[2],Chi2tmp,Chi2red);
}

global void vcTBECHO(double x, double a[], double *y, double dyda[], int na)
{
    int i;
    double fac,ex,arg;
    real vcTotal;

    *y = vcTotalBECHO(x,a);
    vcTotal = vcTotalBECHO(x,a);

    dyda[1] = massDM(x,a) /
    (2.0*a[1]*x*vcTotal);

    dyda[2] = (massDM(x,a) /
            (2.0*a[2]*x*vcTotal))
            *(
                3.0-(x/a[2])*(1.0/IrhoDM(x,a))
                *rexp(-rsqr(x/a[2]))*rsqr(x/a[2])
            );
}

// End: RC BEC-HO Model
// ==========================================



// ==========================================
// Begin: RC PISO Model

local void Model_RCPISO(void)
{
    
    strcpy(gd.model_comment, "RCPISO Model");
    
}

// End: RC PISO Model
// ==========================================



// ==========================================
// Begin: Gaussians Model

void fgauss(double x, double a[], double *y, double dyda[], int na)
{
    int i;
    double fac,ex,arg;
    
    *y=0.0;
    for (i=1;i<=na-1;i+=3) {
        arg=(x-a[i+1])/a[i+2];
        ex=exp(-arg*arg);
        fac=a[i]*ex*2.0*arg;
        *y += a[i]*ex;
        dyda[i]=ex;
        dyda[i+1]=fac/a[i+2];
        dyda[i+2]=fac*arg/a[i+2];
    }
}

#define NPT 100
#define MA 6
#define SPREAD 0.001

global void min_Chi2_gaussians(void)
{
    long idum=(-911);
    int i,*ia,iter,itst,j,k,mfit=MA;
    double alamda,chisq,ochisq,*x,*y,*sig,**covar,**alpha;
    static double a[MA+1]=
    {0.0,5.0,2.0,3.0,2.0,5.0,3.0};
    static double gues[MA+1]=
    {0.0,4.5,2.2,2.8,2.5,4.9,2.8};
    
    ia=nr_ivector(1,MA);
    x=dvector(1,NPT);
    y=dvector(1,NPT);
    sig=dvector(1,NPT);
    covar=dmatrix(1,MA,1,MA);
    alpha=dmatrix(1,MA,1,MA);
// First try a sum of two Gaussians
    for (i=1;i<=NPT;i++) {
        x[i]=0.1*i;
        y[i]=0.0;
        for (j=1;j<=MA;j+=3) {
            y[i] += a[j]*exp(-SQR((x[i]-a[j+1])/a[j+2]));
        }
        y[i] *= (1.0+SPREAD*gasdev(&idum));
        sig[i]=SPREAD*y[i];
    }
    for (i=1;i<=mfit;i++) ia[i]=1;
    for (i=1;i<=MA;i++) a[i]=gues[i];
    for (iter=1;iter<=2;iter++) {
        alamda = -1;
        mrqmin(x,y,sig,NPT,a,ia,MA,covar,alpha,&chisq,fgauss,&alamda);
        k=1;
        itst=0;
        for (;;) {
            printf("\n%s %2d %17s %10.4f %10s %9.2e\n","Iteration #",k,
                   "chi-squared:",chisq,"alamda:",alamda);
            printf("%8s %8s %8s %8s %8s %8s\n",
                   "a[1]","a[2]","a[3]","a[4]","a[5]","a[6]");
            for (i=1;i<=6;i++) printf("%9.4f",a[i]);
            printf("\n");
            k++;
            ochisq=chisq;
            mrqmin(x,y,sig,NPT,a,ia,MA,covar,alpha,&chisq,fgauss,&alamda);
            if (chisq > ochisq)
                itst=0;
            else if (fabs(ochisq-chisq) < 0.1)
                itst++;
            if (itst < 4) continue;
            alamda=0.0;
            mrqmin(x,y,sig,NPT,a,ia,MA,covar,alpha,&chisq,fgauss,&alamda);
            printf("\nUncertainties:\n");
            for (i=1;i<=6;i++) printf("%9.4f",sqrt(covar[i][i]));
            printf("\n");
            printf("\nExpected results:\n");
            printf(" %7.2f %8.2f %8.2f %8.2f %8.2f %8.2f\n",
                   5.0,2.0,3.0,2.0,5.0,3.0);
            break;
        }
        if (iter == 1) {
            printf("press return to continue with constraint\n");
            (void) getchar();
            printf("holding a[2] and a[5] constant\n");
            for (j=1;j<=MA;j++) a[j] += 0.1;
            a[2]=2.0;
            ia[2]=0;
            a[5]=5.0;
            ia[5]=0;
        }
    }
    free_dmatrix(alpha,1,MA,1,MA);
    free_dmatrix(covar,1,MA,1,MA);
    free_dvector(sig,1,NPT);
    free_dvector(y,1,NPT);
    free_dvector(x,1,NPT);
    free_ivector(ia,1,MA);
}
#undef NPT
#undef MA
#undef SPREAD

// End: Gaussians Model
// ==========================================



// ==========================================
// Begin: XXX Model

// End: XXX Model
// ==========================================



