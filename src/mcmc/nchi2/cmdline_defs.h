/*==============================================================================
 HEADER: cmdline_defs.h		[nchi2]
 Written by: Mario A. Rodriguez-Meza
 Starting date: January 2018
 Purpose: Definitions for importing arguments from the command line
 Language: C
 Use: '#include "cmdline_defs.h"
 Use in routines and functions: (main)
 External headers: stdinc.h
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.astro.inin.mx/mar
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains 
 listed below are free from error or suitable for particular applications, 
 and he disclaims all liability from any consequences arising from their use.
 ==============================================================================*/

#ifndef _cmdline_defs_h
#define _cmdline_defs_h

#define HEAD1	"NagBody"
#define HEAD2	"MCMC code"
#define HEAD3	"..."

string defv[] = {  ";"HEAD1": " HEAD2 "\n\t " HEAD3,
    "paramfile=",               ";Parameter input file. Overwrite what follows",
    "inputObsDataFile=",			";File to process", ":in",
    "modeltype=RCBECHO",        ";Model to study", ":model",
    "numparamtofit=2",          ";Number of parameters to fit", ":np",
    "observeddatafile=",        ";Observed data file", ":odf",
    "usingcolumns=1:2:3",       ";Observed data file using columns (x,y,sigma)", ":uc",
    "witherrors=true",          ";Plot error bars", ":werr",
    "errorstype=1",              ";Error bars type", ":errt",
// Minimization parameters:
    "minMethod=amoeba",         ";Minimization method to use", ":mm",
    "options=",                 ";Various control options", ":opt",
    "Version=0.2",              ";Mario A. Rodríguez-Meza 2005-2018",
    NULL,
};

#endif // ! _cmdline_defs_h
