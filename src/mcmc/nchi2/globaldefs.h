/*==============================================================================
 HEADER: globaldefs.h		[nchi2]
 Written by: Mario A. Rodriguez-Meza
 Starting date: January 2018
 Purpose: Definitions of global variables and parameters
 Language: C
 Use: '#include "global_defs.h"
 Use in routines and functions:
 External headers: stdinc.h, data_struc_defs.h
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.astro.inin.mx/mar
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/

#ifndef _globaldefs_h
#define _globaldefs_h


//========================================================
#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifndef NOGNU
#include "../../../General_libs/general/stdinc.h"
#include "../../../General_libs/math/vectmath.h"
#include "../../../General_libs/general/getparam.h"
#include "../../../General_libs/math/mathfns.h"
#include "../../../General_libs/io/inout.h"
#include "../../../General_libs/general/machines.h"
#include "../../../General_libs/math/minpack.h"
#include "../../../General_libs/math/numrec.h"
#include "../../../General_libs/math/mathutil.h"
// #include <strings.h>							// For unix
#include "../../../General_libs/general/strings.h"	// For Visual c
#else
#include "stdinc.h"
#include "vectmath.h"
#include "getparam.h"
#include "mathfns.h"
#include "inout.h"
#include "machines.h"
#include "minpack.h"
#include "numrec.h"
#include "mathutil.h"
// #include <strings.h>							// For unix
#include "strings.h"	// For Visual c
#endif

#include "data_struc_defs.h"
#include "constants_defs.h"
#include "models.h"
#include "protodefs.h"

//========================================================

typedef struct {
    string paramfile;
    string inputObsDataFile;
    string model;
    int numparams;
	string obsdatafile;
    string usingcolumns;
    bool witherrors;
    int errorstype;
    string minMethod;
	string options;
} cmdline_data, *cmdline_data_ptr;

typedef struct {
	real cpuinit;

	string headline0;
	string headline1;
	string headline2;
	string headline3;

    char model_comment[100];

    int minmethod_int;
    char minmethod_comment[100];

    int col1;
    int col2;
    int col3;
    int col4;

	FILE *outlog;

	char mode[2];

} global_data, *global_data_ptr;

global global_data gd;
global cmdline_data cmd;

global real *yout;

global int nparams;
global real *params;
global real **rparams;

// Obs Data structure
typedef struct {
    int no;
    real *xo;
    real *yo;
    real *sigma;
    real *sigmam;
} ObsDataTable, *ObsDataTableptr;

ObsDataTableptr pObs;

#define nObs(x)    (((ObsDataTableptr) (x))->no)
#define xObs(x)    (((ObsDataTableptr) (x))->xo)
#define yObs(x)    (((ObsDataTableptr) (x))->yo)
#define sigmaObs(x)    (((ObsDataTableptr) (x))->sigma)
#define sigmamObs(x)    (((ObsDataTableptr) (x))->sigmam)

#endif // ! _globaldefs_h

