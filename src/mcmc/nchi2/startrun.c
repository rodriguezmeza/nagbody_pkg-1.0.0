/*==============================================================================
 MODULE: startrun.c				[nchi2]
 Written by: Mario A. Rodriguez-Meza
 Starting date: January 2018
 Purpose: routines to initialize the main code
 Language: C
 Use: 'StartRun();'
 Routines and functions:
 Modules, routines and external headers:
 Coments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.astro.inin.mx/mar
 
 Mayor revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/

#include "globaldefs.h"

local void ReadParameterFile(char *);
local void PrintParameterFile(char *);

local void startrun_parameterfile(void);
local void startrun_cmdline(void);
local void ReadParametersCmdline(void);
local void startrun_Common(void);
local void startrun_ParamStat(void);
local void CheckParameters(void);
local void startrun_restorefile(void);

local void Compute_InitialFittingParameters(void);


void StartRun(string head0, string head1, string head2, string head3)
{
    real aTime;
    aTime = cputime();
    
    gd.headline0 = head0; gd.headline1 = head1;
    gd.headline2 = head2; gd.headline3 = head3;
    printf("\n%s\n%s: %s\n\t %s\n", 
		gd.headline0, gd.headline1, gd.headline2, gd.headline3);

    cmd.paramfile = GetParam("paramfile");
    if (!strnull(cmd.paramfile))
		startrun_parameterfile();
	else
		startrun_cmdline();

	StartOutput();
    fprintf(stdout,"Time to StartRun %g\n\n",cputime()-aTime);
    fflush(stdout);
}

local void startrun_parameterfile(void)
{
	ReadParameterFile(cmd.paramfile);
    startrun_ParamStat();
	startrun_Common();
	PrintParameterFile(cmd.paramfile);
}

#define parameter_null	"parameters_null-nagbdmcmc"

local void startrun_cmdline(void)
{
	ReadParametersCmdline();
	startrun_Common();
	PrintParameterFile(parameter_null);
}

local void ReadParametersCmdline(void)
{
    cmd.inputObsDataFile = GetParam("inputObsDataFile");
    cmd.model = GetParam("modeltype");
    cmd.numparams = GetiParam("numparamtofit");
    cmd.obsdatafile = GetParam("observeddatafile");
	cmd.usingcolumns = GetParam("usingcolumns");
    cmd.witherrors = GetbParam("witherrors");
    cmd.errorstype = GetiParam("errorstype");
// Minimization parameters:
    cmd.minMethod = GetParam("minMethod");
//
    cmd.options = GetParam("options");
}

#undef parameter_null

#define logfile			"nchi2.log"

local void startrun_Common(void)
{
    strcpy(gd.mode,"w");
    if(!(gd.outlog=fopen(logfile,gd.mode)))
        error("\nstart_Common: error opening file '%s' \n",logfile);
    
    CheckParameters();
    Compute_InitialFittingParameters();
    minmethod_string_to_int(cmd.minMethod, &gd.minmethod_int);

    if (strnull(cmd.usingcolumns))
        error("startrun_Common: no observed data colummns are selected\n");

    if (cmd.witherrors) {
        switch (cmd.errorstype){
            case 2:
                if (!(sscanf(cmd.usingcolumns, "%d:%d:%d:%d",
                        &gd.col1, &gd.col2, &gd.col3, &gd.col4) == 4))
                    error("\nstartrun_Common: usingcolumns must be in the form c1:c2:c3:c4\n\n");
                fprintf(stdout,"columns: %d %d %d %d\n",gd.col1,gd.col2,gd.col3,gd.col4);
                break;
            case 1:
                if (!(sscanf(cmd.usingcolumns, "%d:%d:%d",
                        &gd.col1, &gd.col2, &gd.col3) == 3))
                    error("\nstartrun_Common: usingcolumns must be in the form c1:c2:c3\n\n");
                fprintf(stdout,"columns: %d %d %d\n",gd.col1,gd.col2,gd.col3);
                break;
            case 0:
                if (!(sscanf(cmd.usingcolumns, "%d:%d", &gd.col1, &gd.col2) == 2))
                    error("\nstartrun_Common: usingcolumns must be in the form c1:c2\n\n");
                fprintf(stdout,"columns: %d %d\n",gd.col1,gd.col2);
                break;
            default: error("\nInputObsDataTable: errortype = %d is absurd\n\n",cmd.errorstype);
        }
    }


    set_model();
/*
#define usrmodel_parameter_null    "usrmodel_parameters_null"
    
    if ( (strcmp(cmd.mgmodel,"USER") == 0) || (strcmp(cmd.mgmodel,"user") == 0))
        if (!strnull(cmd.model_paramfile)) {
            fprintf(stdout,"\n\nUser model :: using parameter file: %s\n",cmd.model_paramfile);
            ReadMGModelParameterFile(cmd.model_paramfile);
            PrintMGModelParameterFile(cmd.model_paramfile);
        } else
            PrintMGModelParameterFile(usrmodel_parameter_null);
#undef usrmodel_parameter_null
*/
}

#undef logfile

local void startrun_ParamStat(void)
{
// Minimization parameters:
    if (GetParamStat("minMethod") & ARGPARAM) {
        cmd.minMethod = GetParam("minMethod");
        fprintf(gd.outlog,"\n\nrunning instead %s minimization method ...\n",
                cmd.minMethod);
    }

    if (GetParamStat("options") & ARGPARAM)
		cmd.options = GetParam("options");
}

local void CheckParameters(void)
{
// Dummy
}

local void Compute_InitialFittingParameters(void)
{
// Dummy until now
    fprintf(gd.outlog,"\nCompute_InitialFittingParameters ...");

    fprintf(gd.outlog,"done.\n");
}


local void ReadParameterFile(char *fname)
{
#define DOUBLE 1
#define STRING 2
#define INT 3
#define BOOLEAN 4
#define MAXTAGS 300

  FILE *fd,*fdout;

  char buf[200],buf1[200],buf2[200],buf3[200];
  int  i,j,nt;
  int  id[MAXTAGS];
  void *addr[MAXTAGS];
  char tag[MAXTAGS][50];
  int  errorFlag=0;

  nt=0;

    SPName(cmd.inputObsDataFile,"inputObsDataFile",200);
    SPName(cmd.model,"modeltype",100);
    RPName(cmd.numparams,"numparamtofit");
    SPName(cmd.obsdatafile,"observeddatafile",100);
    SPName(cmd.usingcolumns,"usingcolumns",100);
    BPName(cmd.witherrors,"witherrors");
    IPName(cmd.errorstype,"errorstype");
// Minimization parameters:
    SPName(cmd.minMethod,"minMethod",100);
	SPName(cmd.options,"options",100);

	if((fd=fopen(fname,"r"))) {
		while(!feof(fd)) {
			fgets(buf,200,fd);
            if(sscanf(buf,"%s%s%s",buf1,buf2,buf3)<1)
                continue;
            if(sscanf(buf,"%s%s%s",buf1,buf2,buf3)<2)
                *buf2='\0';
            if(buf1[0]=='%')
				continue;
            for(i=0,j=-1;i<nt;i++)
                if(strcmp(buf1,tag[i])==0) {
                    j=i;
					tag[i][0]=0;
					break;
				}
			if(j>=0) {
                switch(id[j]) {
					case DOUBLE:
						*((double*)addr[j])=atof(buf2); 
						break;
					case STRING:
						strcpy(addr[j],buf2);
						break;
					case INT:
						*((int*)addr[j])=atoi(buf2);
						break;
					case BOOLEAN:
						if (strchr("tTyY1", *buf2) != NULL) {          
							*((bool*)addr[j])=TRUE;
                        } else 
                            if (strchr("fFnN0", *buf2) != NULL)  {
                                *((bool*)addr[j])=FALSE;
                            } else {
                                error("getbparam: %s=%s not bool\n",buf1,buf2);
                            }
						break;
                }
            } else {
                fprintf(stdout, "Error in file %s: Tag '%s' %s.\n",
					fname, buf1, "not allowed or multiple defined");
                errorFlag=1;
            }
        }
        fclose(fd);
    } else {
        fprintf(stdout,"Parameter file %s not found.\n", fname);
        errorFlag=1;
        exit(1); 
    }
  
    for(i=0;i<nt;i++) {
        if(*tag[i]) {
            fprintf(stdout,
                "Error. I miss a value for tag '%s' in parameter file '%s'.\n",
                tag[i],fname);
            exit(0);
        }
    }
#undef DOUBLE 
#undef STRING 
#undef INT 
#undef BOOLEAN
#undef MAXTAGS
}

#define FMTT	"%-35s%s\n"
#define FMTI	"%-35s%d\n"
#define FMTR	"%-35s%g\n"

local void PrintParameterFile(char *fname)
{
    FILE *fdout;
    char buf[200];
    
    sprintf(buf,"%s%s",fname,"-usedvalues");
    if(!(fdout=fopen(buf,"w"))) {
        fprintf(stdout,"error opening file '%s' \n",buf);
        exit(0);
    } else {
        fprintf(fdout,"%s\n",
		"%-------------------------------------------------------------------");
        fprintf(fdout,"%s %s\n","% Parameter input file for:",gd.headline0);
        fprintf(fdout,"%s\n","%");
        fprintf(fdout,"%s %s: %s\n%s\t    %s\n","%",gd.headline1,gd.headline2,"%",
            gd.headline3);
        fprintf(fdout,"%s\n%s\n",
		"%-------------------------------------------------------------------",
		"%");
        fprintf(fdout,FMTT,"inputObsDataFile",cmd.inputObsDataFile);
        fprintf(fdout,FMTT,"modeltype",cmd.model);
        fprintf(fdout,FMTI,"numparamtofit",cmd.numparams);
        fprintf(fdout,FMTT,"observeddatafile",cmd.obsdatafile);
        fprintf(fdout,FMTT,"usingcolumns",cmd.usingcolumns);
        fprintf(fdout,FMTT,"witherrors",cmd.witherrors ? "true" : "false");
        fprintf(fdout,FMTI,"errorstype",cmd.errorstype);
// Minimization parameters:
        fprintf(fdout,FMTT,"minMethod",cmd.minMethod);
        fprintf(fdout,FMTT,"options",cmd.options);
        fprintf(fdout,"\n\n");
    }
    fclose(fdout);
}

#undef FMTT
#undef FMTI
#undef FMTR

