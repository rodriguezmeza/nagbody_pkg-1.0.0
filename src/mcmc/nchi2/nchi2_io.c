/*==============================================================================
 MODULE: nchi2_io.c		[nchi2]
 Written by: Mario A. Rodriguez-Meza
 Starting date:	January 2018
 Purpose: Routines to drive input and output data
 Language: C
 Use:
 Routines and functions:
 External modules, routines and headers:
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.inin.gob.mx/
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/

#include "globaldefs.h"

local void outputdata(void);

#define fpfnametmp		"obsdatatabletmp.dat"
void InputObsDataTable(void)
{
    stream outstr;
    int i, nObsData;

//
    fprintf(gd.outlog,"\n\nReading observed data from file %s...\n",
            cmd.inputObsDataFile);

    if (cmd.witherrors) {
        switch (cmd.errorstype){

            case 2:
                fprintf(stdout,"Reading four columns (errt=%d)...\n",cmd.errorstype);
                inout_InputData_4c(cmd.inputObsDataFile, gd.col1, gd.col2,
                                   gd.col3, gd.col4, &nObsData);
                break;
            case 1:
                fprintf(stdout,"Reading three columns (errt=%d)...\n",
                        cmd.errorstype);
                inout_InputData_3c(cmd.inputObsDataFile, gd.col1, gd.col2,
                                   gd.col3, &nObsData);
                break;
            case 0:
                    fprintf(stdout,"Reading two columns (errt=%d)...\n",cmd.errorstype);
                    inout_InputData(cmd.inputObsDataFile, gd.col1, gd.col2, &nObsData);
                break;

            default: error("\nUnknown errors type %s\n\n",cmd.errorstype);
        }
    }

    printf("\nnObsData = %d in file: %s\n",nObsData,cmd.inputObsDataFile);
//

    if (nObsData < 1)
        error("\n\nInputObsDataTable: nObsData = %d is absurd\n\n", nObsData);
    
    pObs = (ObsDataTableptr) allocate(sizeof(ObsDataTable));
    nObs(pObs) = nObsData;
    xObs(pObs)=dvector(1,nObsData);
    yObs(pObs)=dvector(1,nObsData);
    sigmaObs(pObs)=dvector(1,nObsData);
    sigmamObs(pObs)=dvector(1,nObsData);

    fprintf(gd.outlog,"nObsData : %d\n", nObs(pObs));

        for (i=1; i<=nObs(pObs); i++) {
        xObs(pObs)[i] = inout_xval[i-1];
        yObs(pObs)[i] = inout_yval[i-1];
        if (cmd.errorstype==0) {
            sigmaObs(pObs)[i] = 1.0;
        } else {
            if (cmd.errorstype==1) {
                sigmaObs(pObs)[i] = inout_zval[i-1];
            } else {
                sigmaObs(pObs)[i] = inout_zval[i-1];
                sigmamObs(pObs)[i] = inout_wval[i-1];
            }
        }
    }

    outstr = stropen(fpfnametmp,"w!");
    if (cmd.errorstype==1 || cmd.errorstype==0) {
        for (i=1; i<=nObs(pObs); i++) {
            fprintf(outstr,"%g %g %g\n",
                xObs(pObs)[i],yObs(pObs)[i],sigmaObs(pObs)[i]);
        }
    } else
        for (i=1; i<=nObs(pObs); i++)
            fprintf(outstr,"%g %g %g %g\n",
                xObs(pObs)[i],yObs(pObs)[i],sigmaObs(pObs)[i],sigmamObs(pObs)[i]);
        
    fclose(outstr);
}
#undef fpfnametmp

void StartOutput(void)
{
    
    fprintf(stdout,"\n  \t -- %s --\n", gd.model_comment);
    //
    fprintf(stdout,"  \t -- %s --\n\n", gd.minmethod_comment);
    if (! strnull(cmd.options))
        fprintf(stdout,"\n\toptions: %s\n", cmd.options);
    
}

void output(void)
{
// Dummy
}


#define SOLFMT \
"%g %g\n"

local void outputdata(void)
{
    // Dummy
    fprintf(stdout,"\nIn outputdata");
}


#define fpfnametmp		"modeltable.dat"
global void plotting_model_table(real (*ymodel)(real, real *))
{
    stream outstr;
    int i, N;
    real x1, x2, dx, x, y;

    outstr = stropen(fpfnametmp,"w!");

    x1 = xObs(pObs)[1];
    x2 = xObs(pObs)[nObs(pObs)];
    N = 10;
    dx = (x2-x1)/((real)(N-1));
    for (i=1; i<=N; i++) {
        x = x1 + dx*((real)(i-1));
        y = ymodel(x,params);
        fprintf(outstr,SOLFMT,x,y);
    }

    fclose(outstr);
}
#undef fpfnametmp
#undef SOLFMT


void CheckStop(void)
{
    // Dummy
    // Set condition to stop
}

void EndRun(void)
{
	fclose(gd.outlog);
	printf("\nFinal CPU time : %lf\n\n", cputime() - gd.cpuinit);
}


