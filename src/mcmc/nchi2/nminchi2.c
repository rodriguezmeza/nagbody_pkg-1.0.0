/*==============================================================================
 MODULE: nminchi2.c				[nchi2]
 Written by: Mario A. Rodriguez-Meza
 Starting date:	January 2018
 Purpose:
 Language: C
 Use:
 Routines and functions:
 External modules, routines and headers:
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.inin.gob.mx/
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/

#include "globaldefs.h"


global void grid_Chi2(real (*ymodel)(real, real *))
{
    real Chi2tmp;
    real Chi2redtmp;
    int i1, i2;
    int N1, N2;
    real dp1, dp2;
    real *p1, *p2, *Chi2red;
    int i;

    if (nObs(pObs)<=nparams) {
        error("\nNumber of observed points is equal or less than the number of parameters!\n\n");
    }

    N1 = 10;
    dp1 = (rparams[1][2]-rparams[1][1])/((real)(N1-1));
    N2 = 10;
    dp2 = (rparams[2][2]-rparams[2][1])/((real)(N2-1));
    fprintf(stdout,"\nGridding parameters space: %d %g %d %g\n",N1, dp1, N2, dp2);

    p1 = dvector(1,N1*N2);
    p2 = dvector(1,N1*N2);
    Chi2red = dvector(1,N1*N2);

    i = 0;
    for (i1=1; i1<=N1; i1++) {
        params[1] = rparams[1][1] + dp1*((real)(i1-1));
        for (i2=1; i2<=N2; i2++) {
            params[2] = rparams[2][1] + dp2*((real)(i2-1));
            Chi2tmp = Chi2(ymodel,
                           params,
                           xObs(pObs), yObs(pObs), sigmaObs(pObs), nObs(pObs));
            Chi2redtmp = Chi2tmp/(nObs(pObs)-nparams);
            i++;
            p1[i] = params[1];
            p2[i] = params[2];
            Chi2red[i] = Chi2redtmp;
        }
    }

    fprintf(stdout,"\nSorting %d Chi2red values",i);
    sort3arrays(i,Chi2red,p1,p2);
    fprintf(stdout,"\nMinimum Chi2red value and its parameters: %g %g %g\n",
            Chi2red[1],p1[1],p2[1]);

    params[1] = p1[1];
    params[2] = p2[1];

    free_dvector(Chi2red,1,N1*N2);
    free_dvector(p2,1,N1*N2);
    free_dvector(p1,1,N1*N2);
}

global real Chi2(real (*ymodel)(real, real *),
                 real params[],
                 real xobs[], real yobs[], real sigma[], int nobs)
{
    real Chi2tmp;
    int i;
    
    Chi2tmp = 0.0;
    for (i=1; i<=nobs; i++) {
        Chi2tmp += rsqr(
                        (yobs[i] - ymodel(xobs[i], params))/sigma[i]
                        );
    }
    
    return Chi2tmp;
}

global void min_Chi2_LevMarq(void (*funcs)(double, double [], double *, double [], int))
{
    int i,*ia,iter,itst,j,k;
    double alamda,chisq,ochisq,**covar,**alpha;

    ia=nr_ivector(1,nparams);
    covar=dmatrix(1,nparams,1,nparams);
    alpha=dmatrix(1,nparams,1,nparams);

    for (i=1;i<=nparams;i++) ia[i]=1;                   // None parameter is fixed...

    fprintf(stdout,"\nInitial parameters:\n");
    for (i=1;i<=nparams;i++) fprintf(stdout,"%g ",params[i]);
    fprintf(stdout,"\n");
    for (iter=1;iter<=1;iter++) {
        alamda = -1;
        mrqmin(xObs(pObs),yObs(pObs),sigmaObs(pObs),nObs(pObs),
               params,ia,nparams,covar,alpha,&chisq,funcs,&alamda);
        k=1;
        itst=0;
        for (;;) {
            printf("\n%s %2d %17s %10.4f %10s %9.2e\n","Iteration #",k,
                   "chi-squared:",chisq,"alamda:",alamda);
            printf("%8s %8s\n",
                   "a[1]","a[2],...");
            for (i=1;i<=nparams;i++) printf("%g ",params[i]);
            printf("\n");
            k++;
            ochisq=chisq;
            mrqmin(xObs(pObs),yObs(pObs),sigmaObs(pObs),nObs(pObs),
                   params,ia,nparams,covar,alpha,&chisq,funcs,&alamda);
            if (chisq > ochisq)
                itst=0;
            else if (fabs(ochisq-chisq) < 0.1)
                itst++;
            if (itst < 4) continue;
            alamda=0.0;
            mrqmin(xObs(pObs),yObs(pObs),sigmaObs(pObs),nObs(pObs),
                   params,ia,nparams,covar,alpha,&chisq,funcs,&alamda);
            printf("\nUncertainties:\n");
            for (i=1;i<=nparams;i++) printf("%g ",rsqrt(covar[i][i]));
            printf("\n");
            break;
        }
    }
    free_dmatrix(alpha,1,nparams,1,nparams);
    free_dmatrix(covar,1,nparams,1,nparams);
    free_ivector(ia,1,nparams);
}


global real P_value(real Chi2, real nu)
{
    real Pvtmp;
    
    Pvtmp = gammp(nu/2.0, Chi2/2.0);
    
    return Pvtmp;
}

global real Q_value(real Chi2, real nu)
{
    real Qvtmp;
    
    Qvtmp = gammq(nu/2.0, Chi2/2.0);
    
    return Qvtmp;
}


// BEGIN :: AMOEBA METHOD

#define FTOL 1.0e-6

#define yfunctomin  vcTotalBECHO

double func_amoeba(double x[])
{
    real functmp;
//    functmp = Chi2(yfunctomin, params, xObs(pObs), yObs(pObs), sigmaObs(pObs), nObs(pObs));
    functmp = Chi2(yfunctomin, x, xObs(pObs), yObs(pObs), sigmaObs(pObs), nObs(pObs));

    return functmp;
//    return 0.6-bessj0(SQR(x[1]-0.5)+SQR(x[2]-0.6)+SQR(x[3]-0.7));
}

global void min_Chi2_amoeba(void (*funcs)(double, double [], double *, double [], int))
{
    int i,nfunc,j,ndim;
    double *x,*y,**p;
    int NP, MP;
    
    NP = nparams;
    MP = NP+1;
    ndim = NP;
    
    printf("\namoeba method of minimization... \n");
//    x=dvector(1,NP);
    y=dvector(1,MP);
    p=dmatrix(1,MP,1,NP);

// i=1
    params[1]= 5555.56;
    p[1][1]= params[1];
    params[2]= 3.3334;
    p[1][2]= params[2];
    y[1]=func_amoeba(params);
    
// i=2
    params[1]= 4555.56;
    p[2][1]= params[1];
    params[2]= 3.3334;
    p[2][2]= params[2];
    y[2]=func_amoeba(params);
    
// i=3
    params[1]= 5555.56;
    p[3][1]= params[1];
    params[2]= 2.3334;
    p[3][2]= params[2];
    y[3]=func_amoeba(params);


//    for (i=1;i<=MP;i++) {
//        for (j=1;j<=NP;j++)
//        params[j]=p[i][j]=(i == (j+1) ? 1.0 : 0.0);
//        y[i]=func_amoeba(params);
//    }
//
    printf("Vertices of initial 3-d simplex and\n");
    printf("function values at the vertices:\n\n");
    printf("%3s %10s %12s %12s %14s\n\n",
           "i","x[i]","y[i]","z[i]","function");
    for (i=1;i<=MP;i++) {
        printf("%3d ",i);
        for (j=1;j<=NP;j++) printf("%12.6f ",p[i][j]);
        printf("%12.6f\n",y[i]);
    }
//
    amoeba(p,y,ndim,FTOL,func_amoeba,&nfunc);
    printf("\nNumber of function evaluations: %3d\n",nfunc);
    printf("Vertices of final 3-d simplex and\n");
    printf("function values at the vertices:\n\n");
    printf("%3s %10s %12s %12s %14s\n\n",
           "i","x[i]","y[i]","z[i]","function");
    for (i=1;i<=MP;i++) {
        printf("%3d ",i);
        for (j=1;j<=NP;j++) printf("%12.6f ",p[i][j]);
        printf("%12.6f\n",y[i]);
    }
//    printf("\nTrue minimum is at (0.5,0.6,0.7)\n");
    free_dmatrix(p,1,MP,1,NP);
    free_dvector(y,1,MP);
//    free_dvector(x,1,NP);
}

#define MP 4
#define NP 3
//#define FTOL 1.0e-6

double func(double x[])
{
    return 0.6-bessj0(SQR(x[1]-0.5)+SQR(x[2]-0.6)+SQR(x[3]-0.7));
}

//int main(void)
void amoeba_drv(void)
{
    int i,nfunc,j,ndim=NP;
    double *x,*y,**p;
    
    printf("\namoeba method of minimization... \n");
    x=dvector(1,NP);
    y=dvector(1,MP);
    p=dmatrix(1,MP,1,NP);
    for (i=1;i<=MP;i++) {
        for (j=1;j<=NP;j++)
            x[j]=p[i][j]=(i == (j+1) ? 1.0 : 0.0);
        y[i]=func(x);
    }
    amoeba(p,y,ndim,FTOL,func,&nfunc);
    printf("\nNumber of function evaluations: %3d\n",nfunc);
    printf("Vertices of final 3-d simplex and\n");
    printf("function values at the vertices:\n\n");
    printf("%3s %10s %12s %12s %14s\n\n",
           "i","x[i]","y[i]","z[i]","function");
    for (i=1;i<=MP;i++) {
        printf("%3d ",i);
        for (j=1;j<=NP;j++) printf("%12.6f ",p[i][j]);
        printf("%12.6f\n",y[i]);
    }
    printf("\nTrue minimum is at (0.5,0.6,0.7)\n");
    free_dmatrix(p,1,MP,1,NP);
    free_dvector(y,1,MP);
    free_dvector(x,1,NP);
//    return 0;
}
#undef NP
#undef MP
#undef FTOL

// END :: AMOEBA METHOD


// BEGIN :: POWELL METHOD
// Driver for routine powell

#define NDIM 3
#define FTOL 1.0e-6

double func_powell(double x[])
{
    return 0.5-bessj0(SQR(x[1]-1.0)+SQR(x[2]-2.0)+SQR(x[3]-3.0));
}

//int main(void)
void powell_drv(void)
{
    int i,iter,j;
    double fret,**xi;
    static double p[]={0.0,1.5,1.5,2.5};

    printf("\nPowell method of minimization... \n");
    xi=dmatrix(1,NDIM,1,NDIM);
    for (i=1;i<=NDIM;i++)
        for (j=1;j<=NDIM;j++)
            xi[i][j]=(i == j ? 1.0 : 0.0);
    powell(p,xi,NDIM,FTOL,&iter,&fret,func_powell);
    printf("Iterations: %3d\n\n",iter);
    printf("Minimum found at: \n");
    for (i=1;i<=NDIM;i++) printf("%12.6f",p[i]);
    printf("\n\nMinimum function value = %12.6f \n\n",fret);
    printf("True minimum of function is at:\n");
    printf("%12.6f %12.6f %12.6f\n",1.0,2.0,3.0);
    free_dmatrix(xi,1,NDIM,1,NDIM);
//    return 0;
}
#undef NDIM
#undef FTOL
//#undef NRANSI

// END :: POWELL METHOD


// BEGIN :: FRPRMN METHOD

//frprmn minimize in N-dimensions by conjugate gradient

/* Driver for routine frprmn */

#define NDIM 3
#define FTOL 1.0e-6
#define PIO2 1.5707963

double func_frprmn(double x[])
{
    return 1.0-bessj0(x[1]-0.5)*bessj0(x[2]-0.5)*bessj0(x[3]-0.5);
}

void dfunc_frprmn(double x[],double df[])
{
    df[1]=bessj1(x[1]-0.5)*bessj0(x[2]-0.5)*bessj0(x[3]-0.5);
    df[2]=bessj0(x[1]-0.5)*bessj1(x[2]-0.5)*bessj0(x[3]-0.5);
    df[3]=bessj0(x[1]-0.5)*bessj0(x[2]-0.5)*bessj1(x[3]-0.5);
}

//int main(void)
void frprmn_drv(void)
{
    int iter,k;
    double angl,fret,*p;

    printf("\nfrprmn method of minimization... \n");
    p=dvector(1,NDIM);
    printf("Program finds the minimum of a function\n");
    printf("with different trial starting vectors.\n");
    printf("True minimum is (0.5,0.5,0.5)\n");
    for (k=0;k<=4;k++) {
        angl=PIO2*k/4.0;
        p[1]=2.0*cos(angl);
        p[2]=2.0*sin(angl);
        p[3]=0.0;
        printf("\nStarting vector: (%6.4f,%6.4f,%6.4f)\n",
               p[1],p[2],p[3]);
        frprmn(p,NDIM,FTOL,&iter,&fret,func_frprmn,dfunc_frprmn);
        printf("Iterations: %3d\n",iter);
        printf("Solution vector: (%6.4f,%6.4f,%6.4f)\n",
               p[1],p[2],p[3]);
        printf("Func. value at solution %14f\n",fret);
    }
    free_dvector(p,1,NDIM);
//    return 0;
}
#undef NDIM
#undef FTOL
#undef PIO2

// END :: FRPRMN METHOD


// BEGIN :: DFPMIN METHOD

// Driver for routine dfpmin

// dfpmin minimize in N-dimensions by variable metric method

static int nfunc,ndfunc;

double func_dfpmin(double x[])
{
    double x1p2sqr=SQR(2.0+x[1]);
    
    nfunc++;
    return 10.0*
    SQR(SQR(x[2])*(3.0-x[1])-SQR(x[1])*(3.0+x[1]))+
    x1p2sqr/(1.0+x1p2sqr);
}

void dfunc_dfpmin(double x[],double df[])
{
    double x1sqr=SQR(x[1]),x2sqr=SQR(x[2]),x1p2=x[1]+2.0;
    double x1p2sqr=SQR(x1p2);
    
    ndfunc++;
    df[1]=20.0*(x2sqr*(3.0-x[1])-x1sqr*(3.0+x[1]))*(-x2sqr-6.0*x[1]-3.0*x1sqr)+
    2.0*x1p2/(1.0+x1p2sqr)-2.0*x1p2*x1p2sqr/SQR((1.0+x1p2sqr));
    df[2]=40.0*(x2sqr*(3.0-x[1])-x1sqr*(3.0+x[1]))*x[2]*(3.0-x[1]);
}

#define NDIM 2
#define GTOL 1.0e-4

//int main(void)
void dfpmin_drv(void)
{
    int iter;
    double *p,fret;
    
    printf("\ndfpmin method of minimization... \n");
    p=dvector(1,NDIM);
    printf("True minimum is at (-2.0,+-0.89442719)\n");
    nfunc=ndfunc=0;
    p[1]=0.1;
    p[2]=4.2;
    printf("Starting vector: (%7.4f,%7.4f)\n",p[1],p[2]);
    dfpmin(p,NDIM,GTOL,&iter,&fret,func_dfpmin,dfunc_dfpmin);
    printf("Iterations: %3d\n",iter);
    printf("Func. evals: %3d\n",nfunc);
    printf("Deriv. evals: %3d\n",ndfunc);
    printf("Solution vector: (%9.6f,%9.6f)\n",p[1],p[2]);
    printf("Func. value at solution %14.6g\n",fret);
    free_dvector(p,1,NDIM);
//    return 0;
}
#undef NDIM
#undef GTOL

// END :: DFPMIN METHOD


// BEGIN :: AMEBSA METHOD

// Driver for routine amebsa

#define NP 4
#define MP 5
#define FTOL 1.0E-6
#define N 4
#define RAD 0.3
#define AUG 2.0

long idum=(-64);

double tfunk(double p[])
{
    int j;
    double q,r,sumd=0.0,sumr=0.0;
    static double wid[N+1]={0.0,1.0,3.0,10.0,30.0};
    
    for (j=1;j<=N;j++) {
        q=p[j]*wid[j];
        r=(double)(q >= 0 ? (int)(q+0.5) : -(int)(0.5-q));
        sumr += q*q;
        sumd += (q-r)*(q-r);
    }
    return 1+sumr*(1+(sumd > RAD*RAD ? AUG : AUG*sumd/(RAD*RAD)));
}

//int main(void)
void amebsa_drv(void)
{
    int i,iiter,iter,j,jiter,ndim=NP,nit;
    double temptr,yb,ybb;
    double **p,*x,*y,*pb;
    static double xoff[NP+1]={0.0,10.0,10.0,10.0,10.0};

    printf("\namebsa method of minimization... \n");
    p=dmatrix(1,MP,1,NP);
    x=dvector(1,NP);
    y=dvector(1,MP);
    pb=dvector(1,NP);
    for (i=1;i<=MP;i++)
        for (j=1;j<=NP;j++) p[i][j]=0.0;
    for (;;) {
        for (j=2;j<=MP;j++) p[j][j-1]=1.0;
        for (i=1;i<=MP;i++) {
            for (j=1;j<=NP;j++) x[j]=(p[i][j] += xoff[j]);
            y[i]=tfunk(x);
        }
        yb=1.0e30;
        printf("Input t, iiter:\n");
        if (scanf("%f %d",&temptr,&iiter) == EOF) break;
        ybb=1.0e30;
        nit=0;
        for (jiter=1;jiter<=100;jiter++) {
            iter=iiter;
            temptr *= 0.8;
            amebsa(p,y,ndim,pb,&yb,FTOL,tfunk,&iter,temptr);
            nit += iiter-iter;
            if (yb < ybb) {
                ybb=yb;
                printf("%6d %10.3e ",nit,temptr);
                for (j=1;j<=NP;j++) printf("%10.5f ",pb[j]);
                printf("%15.7e\n",yb);
            }
            if (iter > 0) break;
        }
        printf("Vertices of final 3-D simplex and\n");
        printf("float values at the vertices:\n");
        printf("%3s %10s %12s %12s %14s\n\n",
               "i","x[i]","y[i]","z[i]","function");
        for (i=1;i<=MP;i++) {
            printf("%3d ",i);
            for (j=1;j<=NP;j++) printf("%12.6f ",p[i][j]);
            printf("%15.7e\n",y[i]);
        }
        printf("%3d ",99);
        for (j=1;j<=NP;j++) printf("%12.6f ",pb[j]);
        printf("%15.7e\n",yb);
    }
    free_dvector(pb,1,NP);
    free_dvector(y,1,MP);
    free_dvector(x,1,NP);
    free_dmatrix(p,1,MP,1,NP);
    printf("Normal completion\n");
//    return 0;
}
#undef NP
#undef MP
#undef FTOL
#undef N
#undef RAD
#undef AUG

// END :: AMEBSA METHOD

