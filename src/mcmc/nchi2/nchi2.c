/*==============================================================================
 MODULE: nchi2.c				[nchi2]
 Written by: Mario A. Rodriguez-Meza
 Starting date:	January 2018
 Purpose:
 Language: C
 Use:
 Routines and functions:
 External modules, routines and headers:
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.inin.gob.mx
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/

#include "globaldefs.h"

local void min_Chi2(void (*funcs)(double, double [], double *, double [], int));

void MainLoop(void)
{
    real pv, qv, Chi2val;

// Setting the model and initial parameters values
    InputObsDataTable();
    set_model();
    grid_Chi2(vcTotalBECHO);
    min_Chi2_LevMarq(vcTBECHO);

    Chi2val = Chi2(vcTotalBECHO, params, xObs(pObs), yObs(pObs), sigmaObs(pObs), nObs(pObs));
    pv = P_value(Chi2val, nObs(pObs)-nparams);
    qv = Q_value(Chi2val, nObs(pObs)-nparams);
    fprintf(stdout,"\nP and Q values and their sum: %g %g %g\n",pv, qv, pv+qv);

    plotting_model_table(vcTotalBECHO);
//
    amoeba_drv();
//    powell_drv();
//    frprmn_drv();
//    dfpmin_drv();
//    amebsa_drv();

//
//    min_Chi2_amoeba(vcTBECHO);
    min_Chi2(vcTBECHO);

}

#define AMOEBA 1
#define NULLMETHOD 0
#define POWELL 2
#define FRPRMN 3
#define DFPMIN 5
#define AMEBSA 6

local void min_Chi2(void (*funcs)(double, double [], double *, double [], int))
{
    switch (gd.minmethod_int) {
        case AMOEBA:
            min_Chi2_amoeba(vcTBECHO);
            break;
            //
        case POWELL:
            min_Chi2_amoeba(vcTBECHO);
            break;
            //
        case FRPRMN:
            min_Chi2_amoeba(vcTBECHO);
            break;
            //
        case DFPMIN:
            min_Chi2_amoeba(vcTBECHO);
            break;
            //
        case AMEBSA:
            min_Chi2_amoeba(vcTBECHO);
            break;
            //
        case NULLMETHOD:
            min_Chi2_amoeba(vcTBECHO);
            break;
            //
        default:
            min_Chi2_amoeba(vcTBECHO);
            break;
    }
}

void minmethod_string_to_int(string method_str,int *method_int)
{
    *method_int=-1;
    if (strcmp(method_str,"amoeba") == 0) {
        *method_int = AMOEBA;
        strcpy(gd.minmethod_comment, "amoeba quadrature method");
    }
    //
    if (strcmp(method_str,"powell") == 0) {
        *method_int = POWELL;
        strcpy(gd.minmethod_comment, "powell quadrature method");
    }
    //
    if (strcmp(method_str,"frprmn") == 0) {
        *method_int = FRPRMN;
        strcpy(gd.minmethod_comment, "frprmn quadrature method");
    }
    //
    if (strcmp(method_str,"dfpmin") == 0) {
        *method_int = DFPMIN;
        strcpy(gd.minmethod_comment, "dfpmin quadrature method");
    }
    //
    if (strcmp(method_str,"amebsa") == 0) {
        *method_int = AMEBSA;
        strcpy(gd.minmethod_comment, "amebsa quadrature method");
    }
    //
    if (strnull(method_str)) {
        *method_int = NULLMETHOD;
        strcpy(gd.minmethod_comment,
               "given null minimization method ... running deafult (amoeba)");
        fprintf(stdout,"\n\tintegration: default integration method (amoeba)...\n");
    }
    //
    if (*method_int == -1) {
        *method_int = AMOEBA;
        strcpy(gd.minmethod_comment,
               "Unknown minimization method ... running deafult (amoeba)");
        fprintf(stdout,"\n\tMinimization: Unknown method... %s ",cmd.minMethod);
        fprintf(stdout,
                "\n\tRunning default minimization method (amoeba)...\n");
    }
}

#undef AMOEBA
#undef POWELL
#undef FRPRMN
#undef DFPMIN
#undef AMEBSA
#undef NULLMETHOD
