/*==============================================================================
	HEADER: models.h			[nchi2]
	Written by: M.A. Rodriguez-Meza
	Starting date: January 2018
	Purpose: proto definitios of some model routines
	Language: C
	Use: '#include "...."
	Use in routines and functions: datanaly_md (main)
	External headers: None
	Comments and notes:
	Info: M.A. Rodriguez-Meza
		Depto. de Fisica, ININ
		Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
		e-mail: marioalberto.rodriguez@inin.gob.mx
		http://www.astro.inin.mx/mar

	Major revisions:
	Copyright: (c) 2005-2018 Mar.  All Rights Reserved
================================================================================
	Legal matters:
	The author does not warrant that the program and routines it contains
	listed below are free from error or suitable for particular applications,
	and he disclaims all liability from any consequences arising from their	use.
==============================================================================*/

#ifndef _models_h
#define _models_h

// ==========================================
// Begin: BEC-HO Model

global real IrhoDM(real x, real params[]);
global real massDM(real x, real params[]);
global real vcDM(real x, real params[]);
global real vcTotalBECHO(real x, real params[]);
global real Chi2(real (*ymodel)(real, real *),
                 real params[],
                 real xobs[], real yobs[], real sigma[], int nobs);
global void test_Chi2_BECHO(void);

global void vcTBECHO(double x, double a[], double *y, double dyda[], int na);


// End: BEC-HO Model
// ==========================================



// ==========================================
// Begin: Gaussians Model

void fgauss(double x, double a[], double *y, double dyda[], int na);
global void min_Chi2_gaussians(void);

// End: Gaussians Model
// ==========================================



// ==========================================
// Begin: XXX Model

// End: XXX Model
// ==========================================



#endif // !_models_h
