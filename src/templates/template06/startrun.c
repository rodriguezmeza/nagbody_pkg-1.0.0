/*==============================================================================
 MODULE: startrun.c				[template06]
 Written by: Mario A. Rodriguez-Meza
 Starting date: January 2018
 Purpose: routines to initialize the main code
 Language: C
 Use: 'StartRun();'
 Routines and functions:
 Modules, routines and external headers:
 Coments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.astro.inin.mx/mar
 
 Mayor revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/

#include "globaldefs.h"
#include "protodefs.h"

local void ReadParameterFile(char *);
local void PrintParameterFile(char *);

local void startrun_parameterfile(void);
local void startrun_cmdline(void);
local void ReadParametersCmdline(void);
local void startrun_Common(void);
local void startrun_ParamStat(void);
local void CheckParameters(void);


void StartRun(string head0, string head1, string head2, string head3)
{
    gd.headline0 = head0; gd.headline1 = head1;
    gd.headline2 = head2; gd.headline3 = head3;
    printf("\n%s\n%s: %s\n\t %s\n", 
		gd.headline0, gd.headline1, gd.headline2, gd.headline3);

	gd.stopflag = 0;

    cmd.paramfile = GetParam("paramfile");
    if (!strnull(cmd.paramfile))
		startrun_parameterfile();
	else
		startrun_cmdline();

	StartOutput();
}

local void startrun_parameterfile(void)
{
	ReadParameterFile(cmd.paramfile);
    startrun_ParamStat();
	startrun_Common();
	PrintParameterFile(cmd.paramfile);
}

#define parameter_null	"parameters_null-nagbdmcmc"

local void startrun_cmdline(void)
{
	ReadParametersCmdline();
	startrun_Common();
	PrintParameterFile(parameter_null);
}

local void ReadParametersCmdline(void)
{
    cmd.x = GetdParam("x");
    cmd.dxstr = GetParam("dx");
    cmd.xstop = GetdParam("xstop");
    cmd.maxnsteps = GetiParam("maxnsteps");
    cmd.outfile = GetParam("out");
	cmd.dxoutstr = GetParam("dxout");
	cmd.dxoutinfostr = GetParam("dxoutinfo");
	cmd.options = GetParam("options");
}

#undef parameter_null

#define logfile			"nagbdmcmc.log"

local void startrun_Common(void)
{
	real dx1, dx2;

    strcpy(gd.mode,"w");

	if(!(gd.outlog=fopen(logfile, gd.mode)))
		error("\nstart_Common: error opening file '%s' \n",logfile);

    gd.dx = (sscanf(cmd.dxstr, "%lf/%lf", &dx1, &dx2) == 2 ?
				dx1/dx2 : atof(cmd.dxstr));
    if ( dx2 == 0. )
        error("\n\nstartrun_Common: dx : dx2 must be finite\n");

    gd.dxout = (sscanf(cmd.dxoutstr, "%lf/%lf", &dx1, &dx2) == 2 ?
				dx1/dx2 : atof(cmd.dxoutstr));
    if ( dx2 == 0. )
        error("\n\nstartrun_Common: dxout : dx2 must be finite\n");
    
    gd.dxoutinfo = (sscanf(cmd.dxoutinfostr, "%lf/%lf", &dx1, &dx2) == 2 ?
				dx1/dx2 : atof(cmd.dxoutinfostr));
    if ( dx2 == 0. )
        error("\n\nstartrun_Common: dxoutinfo : dx2 must be finite\n");

    CheckParameters();
    gd.xnow = cmd.x;
    gd.nstep = 0;
    gd.xout = gd.xnow;
    gd.xoutinfo = gd.xnow;
}

#undef logfile


local void startrun_ParamStat(void)
{
// PONER LOS PARAMETROS QUE PUEDEN SER DADOS DESPUES DE LEER EL ARCHIVO DE PARAMETROS
// ES DECIR, QUE PUEDEN MODIFICAR LOS VALORES ESTABLECIDOS EN EL ARCHIVO DE PARAMETROS
//
// En la linea de comandos the options must be after paramfile
// or using "paramfile=paramnamefile"
//
	real dx1, dx2;

	if (GetParamStat("x") & ARGPARAM) 
		cmd.x = GetdParam("x");
	if (GetParamStat("dx") & ARGPARAM) {
		cmd.dxstr = GetParam("dx");
		gd.dx = (sscanf(cmd.dxstr, "%lf/%lf", &dx1, &dx2) == 2 ?
                    dx1/dx2 : atof(cmd.dxstr));
		if ( dx2 == 0. )
			error("\n\nstartrun_ParamStat: dtime : dx2 must be finite\n");
	}
	if (GetParamStat("xstop") & ARGPARAM) 
		cmd.xstop = GetdParam("xstop");

	if (GetParamStat("maxnsteps") & ARGPARAM) 
		cmd.maxnsteps = GetiParam("maxnsteps");

	if (GetParamStat("out") & ARGPARAM)
		cmd.outfile = GetParam("out");

	if (GetParamStat("dxout") & ARGPARAM) {
		cmd.dxoutstr = GetParam("dxout");
		gd.dxout = (sscanf(cmd.dxoutstr, "%lf/%lf", &dx1, &dx2) == 2 ?
                    dx1/dx2 : atof(cmd.dxoutstr));
		if ( dx2 == 0. )
			error("\n\nstartrun_ParamStat: dxout : dx2 must be finite\n");
	}

	if (GetParamStat("dxoutinfo") & ARGPARAM) {
		cmd.dxoutinfostr = GetParam("dxoutinfo");
		gd.dxoutinfo = (sscanf(cmd.dxoutinfostr, "%lf/%lf", &dx1, &dx2) == 2 ?
                    dx1/dx2 : atof(cmd.dxoutinfostr));
		if ( dx2 == 0. )
			error("\n\nstartrun_ParamStat: dxoutinfo : dx2 must be finite\n");
	}
    
	if (GetParamStat("options") & ARGPARAM)
		cmd.options = GetParam("options");

}

local void CheckParameters(void)
{
// PONER LAS CONDICIONANTES PARA PROBAR PARAMETROS ...
//
	if (gd.dx == 0)
		error("CheckParameters: absurd value for dx\n");

    if(cmd.x == cmd.xstop)
        error("\n\nstartrun_Common: x and xstop must be different\n");

    if (cmd.maxnsteps < 1)
        error("CheckParameters: absurd value for maxnsteps\n");
    
	if (gd.dxout == 0)
		error("CheckParameters: absurd value for dxout\n");
    
	if (gd.dxoutinfo == 0)
		error("CheckParameters: absurd value for dxoutinfo\n");

}


local void ReadParameterFile(char *fname)
{
#define DOUBLE 1
#define STRING 2
#define INT 3
#define BOOLEAN 4
#define MAXTAGS 300

  FILE *fd,*fdout;

  char buf[200],buf1[200],buf2[200],buf3[200];
  int  i,j,nt;
  int  id[MAXTAGS];
  void *addr[MAXTAGS];
  char tag[MAXTAGS][50];
  int  errorFlag=0;

  nt=0;

	RPName(cmd.x,"x");
	SPName(cmd.dxstr,"dx",100);
	RPName(cmd.xstop,"xstop");
	IPName(cmd.maxnsteps,"maxnsteps");
	SPName(cmd.outfile,"out",100);
	SPName(cmd.dxoutstr,"dxout",100);
	SPName(cmd.dxoutinfostr,"dxoutinfo",100);
	SPName(cmd.options,"options",100);

	if((fd=fopen(fname,"r"))) {
		while(!feof(fd)) {
			fgets(buf,200,fd);
            if(sscanf(buf,"%s%s%s",buf1,buf2,buf3)<1)
                continue;
            if(sscanf(buf,"%s%s%s",buf1,buf2,buf3)<2)
                *buf2='\0';
            if(buf1[0]=='%')
				continue;
            for(i=0,j=-1;i<nt;i++)
                if(strcmp(buf1,tag[i])==0) {
                    j=i;
					tag[i][0]=0;
					break;
				}
			if(j>=0) {
                switch(id[j]) {
					case DOUBLE:
						*((double*)addr[j])=atof(buf2); 
						break;
					case STRING:
						strcpy(addr[j],buf2);
						break;
					case INT:
						*((int*)addr[j])=atoi(buf2);
						break;
					case BOOLEAN:
						if (strchr("tTyY1", *buf2) != NULL) {          
							*((bool*)addr[j])=TRUE;
                        } else 
                            if (strchr("fFnN0", *buf2) != NULL)  {
                                *((bool*)addr[j])=FALSE;
                            } else {
                                error("getbparam: %s=%s not bool\n",buf1,buf2);
                            }
						break;
                }
            } else {
                fprintf(stdout, "Error in file %s: Tag '%s' %s.\n",
					fname, buf1, "not allowed or multiple defined");
                errorFlag=1;
            }
        }
        fclose(fd);
    } else {
        fprintf(stdout,"Parameter file %s not found.\n", fname);
        errorFlag=1;
        exit(1); 
    }
  
    for(i=0;i<nt;i++) {
        if(*tag[i]) {
            fprintf(stdout,
                "Error. I miss a value for tag '%s' in parameter file '%s'.\n",
                tag[i],fname);
            exit(0);
        }
    }
#undef DOUBLE 
#undef STRING 
#undef INT 
#undef BOOLEAN
#undef MAXTAGS
}

#define FMTT	"%-35s%s\n"
#define FMTI	"%-35s%d\n"
#define FMTR	"%-35s%g\n"

local void PrintParameterFile(char *fname)
{
    FILE *fdout;
    char buf[200];
    
    sprintf(buf,"%s%s",fname,"-usedvalues");
    if(!(fdout=fopen(buf,"w"))) {
        fprintf(stdout,"error opening file '%s' \n",buf);
        exit(0);
    } else {
        fprintf(fdout,"%s\n",
		"%-------------------------------------------------------------------");
        fprintf(fdout,"%s %s\n","% Parameter input file for:",gd.headline0);
        fprintf(fdout,"%s\n","%");
        fprintf(fdout,"%s %s: %s\n%s\t    %s\n","%",gd.headline1,gd.headline2,"%",
            gd.headline3);
        fprintf(fdout,"%s\n%s\n",
		"%-------------------------------------------------------------------",
		"%");
        fprintf(fdout,FMTR,"x",cmd.x);
        fprintf(fdout,FMTT,"dx",cmd.dxstr);
        fprintf(fdout,FMTR,"xstop",cmd.xstop);
        fprintf(fdout,FMTI,"maxnsteps",cmd.maxnsteps);
        fprintf(fdout,FMTT,"out",cmd.outfile);
        fprintf(fdout,FMTT,"dxout",cmd.dxoutstr);
        fprintf(fdout,FMTT,"dxoutinfo",cmd.dxoutinfostr);
        fprintf(fdout,FMTT,"options",cmd.options);
        fprintf(fdout,"\n\n");
    }
    fclose(fdout);
}

#undef FMTT
#undef FMTI
#undef FMTR

