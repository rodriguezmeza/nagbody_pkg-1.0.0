/*==============================================================================
 HEADER: cmdline_defs.h		[template06]
 Written by: Mario A. Rodriguez-Meza
 Starting date: January 2018
 Purpose: Definitions for importing arguments from the command line
 Language: C
 Use: '#include "cmdline_defs.h"
 Use in routines and functions: (main)
 External headers: stdinc.h
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.astro.inin.mx/mar
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains 
 listed below are free from error or suitable for particular applications, 
 and he disclaims all liability from any consequences arising from their use.
 ==============================================================================*/

#ifndef _cmdline_defs_h
#define _cmdline_defs_h

#define HEAD1	"NagBody"
#define HEAD2	"Template Code for Bessel J0, J1, J2 y J3"
#define HEAD3	"..."

string defv[] = {  ";"HEAD1": " HEAD2 "\n\t " HEAD3,
    "paramfile=",			";Parameter input file. Overwrite what follows",
    "x=0.0",                ";x value",
    "dx=2/5",               ";dx advance step",
    "xstop=10.0",			";x value to stop computation",
    "maxnsteps=100",        ";Maximum number of steps", ":maxn",
    "out=sols.dat",          ";Output file of the solutions", ":o",
    "dxout=2/5",			";Data output x step", ":dxo",
    "dxoutinfo=2/5",		";Info output x step", ":dxoinfo",
    "options=",				";Various control options", ":opt",
    "Version=0.2",			";Mario A. Rodríguez-Meza 2005-2018",
    NULL,
};

#endif // ! _cmdline_defs_h
