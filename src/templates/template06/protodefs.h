/*==============================================================================
 HEADER: protodefs.h				[template06]
 Written by: Mario A. Rodriguez-Meza
 Starting date: January 2018
 Purpose: Definitions of global prototypes
 Language: C
 Use: '#include "protodefs.h"
 Use in routines and functions:
 External headers: None
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.astro.inin.mx/mar
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/

#ifndef _protodefs_h
#define _protodefs_h

void output(void);

void MainLoop(void);
void StartRun(string, string, string, string);
void StartOutput(void);
void CheckStop(void);
void EndRun(void);

#endif // ! _protodefs_h
