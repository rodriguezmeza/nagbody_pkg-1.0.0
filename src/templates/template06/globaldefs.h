/*==============================================================================
 HEADER: globaldefs.h		[template06]
 Written by: Mario A. Rodriguez-Meza
 Starting date: January 2018
 Purpose: Definitions of global variables and parameters
 Language: C
 Use: '#include "global_defs.h"
 Use in routines and functions:
 External headers: stdinc.h, data_struc_defs.h
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.astro.inin.mx/mar
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/

#ifndef _globaldefs_h
#define _globaldefs_h

//===============================================
#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifndef NOGNU
#include "../../../General_libs/general/stdinc.h"
#include "../../../General_libs/math/vectmath.h"
#include "../../../General_libs/general/getparam.h"
#include "../../../General_libs/math/mathfns.h"
#include "../../../General_libs/general/machines.h"
#include "../../../General_libs/math/numrec.h"
#include "../../../General_libs/general/strings.h"
#else
#include "stdinc.h"
#include "vectmath.h"
#include "getparam.h"
#include "mathfns.h"
#include "machines.h"
#include "numrec.h"
#include "strings.h"
#endif

#include "data_struc_defs.h"
//===============================================



typedef struct {
	real x;
	string dxstr;
	real xstop;
    int maxnsteps;

	string outfile;
	string dxoutstr;
	string dxoutinfostr;

	string options;
	string paramfile;

} cmdline_data, *cmdline_data_ptr;

typedef struct {
	real cpuinit;
	real dx;
    real dxout;
    real dxoutinfo;

	string headline0;
	string headline1;
	string headline2;
	string headline3;

	int nstep;

	FILE *outlog;
	FILE *outstr_sols;

	int stopflag;
    
    real xnow;
    real xout;
    real xoutinfo;

	char mode[2];

} global_data, *global_data_ptr;

global global_data gd;
global cmdline_data cmd;

global real *yout;


#endif // ! _globaldefs_h

