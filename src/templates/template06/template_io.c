/*==============================================================================
 MODULE: nagbdmcmc_io.c		[template06]
 Written by: Mario A. Rodriguez-Meza
 Starting date:	January 2018
 Purpose: Routines to drive input and output data
 Language: C
 Use:
 Routines and functions:
 External modules, routines and headers:
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.astro.inin.mx/mar
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/


#include "globaldefs.h"
#include "protodefs.h"

local void outputdata(void);


void StartOutput(void)
{

    if(!(gd.outstr_sols=fopen(cmd.outfile,gd.mode)))
    {
        error("can't open file `%s`\n",cmd.outfile);
    }
    fprintf(gd.outstr_sols,"%1s%4s%8s%8s%8s%8s%8s",
			"#","nstep","x","j0","j1","j2","j3");
    fprintf(gd.outstr_sols,"\n");
    fprintf(gd.outstr_sols,
			"%1s%4s%10s%8s%8s%8s%8s",
			"#","<1>","<2>","<3>","<4>","<5>","<6>");
    fprintf(gd.outstr_sols,"\n");

    fprintf(stdout,"\n%8s%8s%8s", "nstep", "x", "dx");
    fprintf(stdout,"%7s%10s%8s\n", "dxout","dxoutinfo","xstop");
    fprintf(stdout,"%8d%8.2f%8.4f%8.4f%8.4f%8.4f",
            gd.nstep,cmd.x,gd.dx,gd.dxout,gd.dxoutinfo,cmd.xstop);
    if (! strnull(cmd.options))
        fprintf(stdout,"\n\toptions: %s\n", cmd.options);

	fprintf(stdout,"\n\n%16s %5s %12s %12s %12s\n",
            "Bessel function:","j0","j1","j2","j3");
    
}

void output(void)
{
    real xeff;
    int j;

	xeff = gd.xnow + gd.dx/10.0;

    if (xeff >= gd.xoutinfo) {
		fprintf(stdout,"\nnstep = %d  dx = %8.6f  x = %12.6f\n",
                gd.nstep,gd.dx,gd.xnow);
		fprintf(stdout,"%12s","rk4:");
		fprintf(stdout,"\n%12s %12.6f %12.6f %12.6f %12.6f\n","actual:",
               bessj0(gd.xnow),bessj1(gd.xnow),bessj(2,gd.xnow),bessj(3,gd.xnow));
		gd.xoutinfo += gd.dxoutinfo;
	}


    if (! strnull(cmd.outfile) && xeff >= gd.xout) {
        fprintf(gd.outlog,"\n\tcol-ascii format output\n");
        outputdata();
        gd.xout += gd.dxout;
    }
}


#define SOLFMT \
"%5d %8.4f %7.4f %7.4f %7.4f %7.4f"

local void outputdata(void)
{
	fprintf(gd.outstr_sols, SOLFMT,
            gd.nstep, gd.xnow,
            bessj0(gd.xnow),bessj1(gd.xnow),bessj(2,gd.xnow),bessj(3,gd.xnow)
            );
	fprintf(gd.outstr_sols,"\n");
}

#undef SOLFMT

void CheckStop(void)
{
    char   buf[200];
	FILE *fd;
	double cpudt;

    if (gd.nstep > cmd.maxnsteps) {
		gd.stopflag = 1;
        fprintf(stdout,"\n\nMaximum number of steps reached...");
        fprintf(stdout,"\nStopping...\n\n");
    }
}

void EndRun(void)
{
    char   buf[200];
	FILE *fd;

	fclose(gd.outlog);
	fclose(gd.outstr_sols);
	printf("\nFinal CPU time : %lf\n\n", cputime() - gd.cpuinit);
}



