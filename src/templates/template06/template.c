/*==============================================================================
 MODULE: nagbdmcmc.c				[template06]
 Written by: Mario A. Rodriguez-Meza
 Starting date:	January 2018
 Purpose:
 Language: C
 Use:
 Routines and functions:
 External modules, routines and headers:
 Comments and notes:
 Info: Mario A. Rodriguez-Meza
 Depto. de Fisica, ININ
 Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
 e-mail: marioalberto.rodriguez@inin.gob.mx
 http://www.astro.inin.mx/mar
 
 Major revisions:
 Copyright: (c) 2005-2018 Mar.  All Rights Reserved
 ================================================================================
 Legal matters:
 The author does not warrant that the program and routines it contains
 listed below are free from error or suitable for particular applications,
 and he disclaims all liability from any consequences arising from their	use.
 ==============================================================================*/

#include "globaldefs.h"
#include "protodefs.h"


local void stepsystem(void);

void MainLoop(void)
{
//    int i;

    if (gd.nstep == 0) {
        output();
    }

    if (gd.dx != 0.0)
        while (cmd.xstop - gd.xnow > 0.01*gd.dx) {
            stepsystem();
            output();
			CheckStop();
			if (gd.stopflag) break;
        }

}

local void stepsystem(void)
{
	int i;

    gd.nstep++;
    gd.xnow = gd.xnow + gd.dx;
}


