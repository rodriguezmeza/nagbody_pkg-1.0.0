/*==============================================================================
	HEADER: cmdlinedefs.h			[nxls]
	Written by: Mario A. Rodriguez-Meza
	Starting date: May 2006
	Purpose: Definitions for importing arguments from the command line
	Language: C
	Use: '#include "cmdlinedefs.h"
	Use in routines and functions: (main)
	External headers: stdinc.h
	Comments and notes:
	Info: M.A. Rodriguez-Meza
		Depto. de Fisica, ININ
		Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
		e-mail: marioalberto.rodriguez@inin.gob.mx
		http://www.astro.inin.mx/mar

	Major revisions: November 2008; February 2018;
	Copyright: (c) 2005-2018 Mar.  All Rights Reserved
================================================================================
	Legal matters:
	The author does not warrant that the program and routines it contains
	listed below are free from error or suitable for particular applications,
	and he disclaims all liability from any consequences arising from their use.
==============================================================================*/

#ifndef _cmdline_defs_h
#define _cmdline_defs_h

//#include "../../../General_libs/general/stdinc.h"

#define HEAD1	"NagBody"
#define HEAD2	"modifying and statistics of data ..."
#define HEAD3	"-- ... --"

string defv[] = {  ";"HEAD1": " HEAD2 "\n\t " HEAD3,
    "inputfile=",			";File to process (in column format)", ":in",
    "out=datamod.dat",         ";Output file", ":o",
    "outfmt=col-ascii",     ";Output file formats", ":ofmt",
	"usingcolumns=1:2:3",		";Columns to process", ":uc",
	"operation=sum",		";Operation on columns", ":op",
	"factor=1",             ";Factor to use when adding or substracting columns", ":fac",
	"xrange=auto",		";Range in x-axis", ":xr",
	"yrange=auto",		";Range in y-axis", ":yr",
    "options=",				";Various control options", ":opt",
    "paramfile=",			";Parameter input file. Use instead command line parameters",
    "Version=0.2",			";Mario A. Rodriguez-Meza 2005-2018",
    NULL,
};

// Note: usingcolumns debe ser tres columnas 
//y deben estar witherrorbars=true y errorbarstype=0 sino no funciona

// La primer columna es la referencia y ninguna operacion se hace sobre ella.

#endif // ! _cmdline_defs_h
