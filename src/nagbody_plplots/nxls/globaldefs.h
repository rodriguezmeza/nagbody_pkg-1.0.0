/*==============================================================================
	HEADER: global_defs.h		[nxls]
	Written by: Mario A. Rodriguez-Meza
	Starting date: May 2006
	Purpose: Definitions of global variables and parameters
	Language: C
	Use: '#include "global_defs.h"
	Use in routines and functions:
	External headers:
	Comments and notes:
	Info: M.A. Rodriguez-Meza,
		Depto. de Fisica, ININ,
		Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico.
		e-mail: marioalberto.rodriguez@inin.gob.mx
		http://www.astro.inin.mx/mar

	Major revisions: November 2008; February 2018;
	Copyright: (c) 2005-2018 Mar.  All Rights Reserved.
================================================================================
	Legal matters:
	The author does not warrant that the program and routines it contains
	listed below are free from error or suitable for particular applications,
	and he disclaims all liability from any consequences arising from their
	use.
==============================================================================*/

#ifndef _global_defs_h
#define _global_defs_h

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#ifndef NOGNU
#include "../../../General_libs/general/stdinc.h"
#include "../../../General_libs/math/vectdefs.h"
#include "../../../General_libs/general/getparam.h"
#include "../../../General_libs/io/inout.h"
#include "../../../General_libs/math/mathfns.h"
#include "../../../General_libs/general/lic.h"
#else
#include "stdinc.h"
#include "vectdefs.h"
#include "getparam.h"
#include "inout.h"
#include "mathfns.h"
#include "lic.h"
#endif

#include "protodefs.h"


// -----Block of command line definitions---------------------------------------

typedef struct {										

	string inputfile;
	string outfile;
	string outfilefmt;

	string usingcolumns;
    string operation;
    real factor;
	string xrange;
	string yrange;

	string options;
    string paramfile;

} cmdline_data, *cmdline_data_ptr;


//------------------------------------------------------------------------------

#define MAXLINES	1000

typedef struct {

	int nfiles;
	char *filenames[40];

	int nwitherrorbars;
	bool errorbars[MAXLINES];

    int operation_int;
	char operation_comment[100];

// ----- Block of definitions for xrange and yrange ----------------------------
	bool x_auto;
	bool y_auto;
	real xmin;
	real xmax;
	real ymin;
	real ymax;

// ----- Block of definitions for usingcolumns ---------------------------------
	int *vcol1;
	int *vcol2;
	int *vcol3;
	int *vcol4;

// ----- Block of definitions for headlines -----------------------------------
	string headline0;
	string headline1;
	string headline2;
	string headline3;
	string comment;			
	FILE *outlog;

// ----- Other definitions -----------------------------------------------------

	int npoint;

	long seed;

	real *xval;
	real *yval;
	real *zval;
	real *yminval;
	real *ymaxval;

	real cpuinit;

} global_data, *global_data_ptr;

#if !defined(global)			// Posicion original de la definicion "global"
#define global extern
#endif

// DEFINICION GLOBAL DE ESTRUCTURAS DE DATOS...
global global_data gd;
global cmdline_data cmd;


// MACROS - Definitions to add a parameter in the scheme of parameterfile ------

#define IPName(param,paramtext)										\
{strcpy(tag[nt],paramtext);										\
addr[nt]=&(param);												\
id[nt++]=INT;}

#define RPName(param,paramtext)										\
{strcpy(tag[nt],paramtext);										\
addr[nt]=&param;													\
id[nt++]=DOUBLE;}

#define BPName(param,paramtext)										\
{strcpy(tag[nt],paramtext);										\
addr[nt]=&param;													\
id[nt++]=BOOLEAN;}

#define SPName(param,paramtext,n)									\
{strcpy(tag[nt],paramtext);										\
param=(string) malloc(n);											\
addr[nt]=param;													\
id[nt++]=STRING;}

// -----------------------------------------------------------------------------



#endif // ! _global_defs_h

