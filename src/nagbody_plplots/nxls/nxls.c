/* =============================================================================
	MODULE: nxls.c				[nxls]
	Written by: Mario A. Rodriguez-Meza
	Starting date:	May 2006
	Purpose:
	Language: C
	Use:
	Routines and functions:
	External modules, routines and headers:
	Comments and notes:
	Info: Mario A. Rodriguez-Meza
		Depto. de Fisica, ININ
		Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
		e-mail: marioalberto.rodriguez@inin.gob.mx
		http://www.astro.inin.mx/mar

	Major revisions: November 2008; February 2018;
	Copyright: (c) 2005-2018 Mar.  All Rights Reserved.
================================================================================
	Legal matters:
	The author does not warrant that the program and routines it contains
	listed below are free from error or suitable for particular applications,
	and he disclaims all liability from any consequences arising from their
	use.
==============================================================================*/

#include "globaldefs.h"
//#include "protodefs.h"

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include <string.h> // To avoid
// warning: implicit declaration of function ‘strcmp’ 'strcpy'... [-Wimplicit-function-declaration]


local void plot2d_normal(void);

void nxlsDriver(void)
{
    plot2d_normal(); 
}

#define SUM         0
#define MUL         1
#define DIV         2
#define SUBTRACTION 3
#define NULLOPERATION 1000

local void plot2d_normal(void)
{
    stream outstr;
    real optcs;
  int i, ifile;
  real *xnc[gd.nfiles], *ync[gd.nfiles], *yminnc[gd.nfiles], *ymaxnc[gd.nfiles];
  real *yminnctmp[gd.nfiles];
   real *znc[gd.nfiles];
  int vnpoint[gd.nfiles];

//	printf("\n\nNormal plotting ...\n\n");
// COMIENZA BLOQUE DE LECTURA DE ARCHIVOS Y GENERACION DEL ARREGLO DE X Y Y'S


	for (ifile=0; ifile<gd.nfiles; ifile++) {

					InputData_3c(gd.filenames[ifile], gd.vcol1[ifile], gd.vcol2[ifile],
						gd.vcol3[ifile], &vnpoint[ifile]);

		printf("\nvnpoint[%d] = %d\n",ifile,vnpoint[ifile]);

		gd.npoint=vnpoint[ifile];

		xnc[ifile] = &gd.xval[0]; ync[ifile]=&gd.yval[0]; znc[ifile]=&gd.zval[0];

	}

printf("\nscaling done ...\n");

printf("\nstarting the operation on columns ...\n");

    outstr = stropen(cmd.outfile, "w!");
    ifile = 0;
    switch (gd.operation_int) {
        case SUM:
            for (i = 0; i < vnpoint[ifile]; i++) {
                optcs = ync[ifile][i] + cmd.factor*znc[ifile][i];
//                optcs = ync[ifile][i] + znc[ifile][i];
//                fprintf(outstr,"%d %g %g %g\n",
                        fprintf(outstr,"%d %g %g %g %g\n",
//                        i+1,xnc[ifile][i],ync[ifile][i],optcs);
                        i+1,xnc[ifile][i],ync[ifile][i],znc[ifile][i],optcs);
            }
            break;
            
        case MUL:
            for (i = 0; i < vnpoint[ifile]; i++) {
                optcs = ync[ifile][i] * znc[ifile][i];
//                optcs = ync[ifile][i] * znc[ifile][i];
//                fprintf(outstr,"%d %g %g %g\n",
                        fprintf(outstr,"%d %g %g %g %g\n",
//                        i+1,xnc[ifile][i],ync[ifile][i],optcs);
                i+1,xnc[ifile][i],ync[ifile][i],znc[ifile][i],optcs);
            }
            break;
            
        case DIV:
            for (i = 0; i < vnpoint[ifile]; i++) {
                optcs = ync[ifile][i] / znc[ifile][i];
//                optcs = ync[ifile][i] / znc[ifile][i];
//                fprintf(outstr,"%d %g %g %g\n",
//                        i+1,xnc[ifile][i],ync[ifile][i],optcs);
                fprintf(outstr,"%d %g %g %g %g\n",
                        i+1,xnc[ifile][i],ync[ifile][i],znc[ifile][i],optcs);
            }
            break;
            
        case SUBTRACTION:
            for (i = 0; i < vnpoint[ifile]; i++) {
                optcs = ync[ifile][i] - cmd.factor*znc[ifile][i];
//                optcs = ync[ifile][i] - znc[ifile][i];
//                fprintf(outstr,"%d %g %g %g\n",
//                        i+1,xnc[ifile][i],ync[ifile][i],optcs);
                fprintf(outstr,"%d %g %g %g %g\n",
                        i+1,xnc[ifile][i],ync[ifile][i],znc[ifile][i],optcs);
            }
            break;
            
        default:
            fprintf(stdout,"\nUnknown operation : using default (SUM)\n");
            for (i = 0; i < vnpoint[ifile]; i++) {
                optcs = ync[ifile][i] + znc[ifile][i];
//                optcs = ync[ifile][i] + znc[ifile][i];
//                fprintf(outstr,"%d %g %g %g\n",
//                        i+1,xnc[ifile][i],ync[ifile][i],optcs);
                fprintf(outstr,"%d %g %g %g %g\n",
                        i+1,xnc[ifile][i],ync[ifile][i],znc[ifile][i],optcs);
            }
            break;
    }
    printf(" done ...\n");
    fclose(outstr);

}

void operation_string_to_int(string operation_str,int *operation_int)
{
    *operation_int=-1;

    if (strcmp(operation_str,"sum") == 0) {
        *operation_int = SUM;
        strcpy(gd.operation_comment, "Sum");
        return;
    } 
    
    if (strcmp(operation_str,"mul") == 0) {
        *operation_int = MUL;
        strcpy(gd.operation_comment, "Mul");
        return;
    } 

    if (strcmp(operation_str,"div") == 0) {
        *operation_int = DIV;
        strcpy(gd.operation_comment, "Div");
        return;
    }
    
    if (strcmp(operation_str,"sub") == 0) {
        *operation_int = SUBTRACTION;
        strcpy(gd.operation_comment, "Subtraction");
        return;
    } 
    
    if (strnull(operation_str)) {              
        *operation_int = NULLOPERATION;
        strcpy(gd.operation_comment, 
                "null operation ... running deafult (sum)");
        fprintf(stdout,"\n\toperation: default operation (sum)...\n");
    } else {
            
            fprintf(stdout,"\n\toperation: Unknown operation... %s ",cmd.operation);
            fprintf(stdout,
                    "\n\trunning default operation (sum)...\n"); 
        }
}

#undef SUM
#undef MUL
#undef DIV
#undef SUBTRACTION
#undef NULLOPERATION

