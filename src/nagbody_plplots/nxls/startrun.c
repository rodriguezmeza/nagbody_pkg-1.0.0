/*==============================================================================
	MODULE: start_run.c				[nxls]
	Written by: Mario A. Rodriguez-Meza
	Starting date: May 2006
	Purpose: routines to initialize the main code
	Language: C
	Use: 'StartRun();'
	Routines and functions:
	Modules, routines and external headers:
	Coments and notes:
	Info: M.A. Rodriguez-Meza
		Depto. de Fisica, ININ
		Apdo. Postal 18-1027 Mexico D.F. 11801 Mexico
		e-mail: marioalberto.rodriguez@inin.gob.mx
		http://www.astro.inin.mx/mar

	Mayor revisions: November 2008; February 2018;
	Copyright: (c) 2005-2018 Mar.  All Rights Reserved
================================================================================
	Legal matters:
	The author does not warrant that the program and routines it contains
	listed below are free from error or suitable for particular applications,
	and he disclaims all liability from any consequences arising from their use.
==============================================================================*/



#include "globaldefs.h"
//#include "protodefs.h"

local void StartRunParameterfile(void);
local void StartRunCmdline(void);
local void ReadParameterFile(char *);
local void PrintParameterFile(char *);
local void PrintParametersLog(void);

local void ReadParametersCmdline(void);
local void startrun_Common(void);
local void startrun_ParamStat(void);
local void CheckParameters(void);

local void scanbOption(string, bool *, int *, int, int, string);
local void scaniOption(string, int *, int *, int, int, string);

#define logfile			"nxls.log"

void StartRun(string head0, string head1, string head2, string head3)
{
    gd.headline0 = head0; gd.headline1 = head1;
    gd.headline2 = head2; gd.headline3 = head3;
    printf("\n%s\n%s: %s\n\t %s\n", 
		gd.headline0, gd.headline1, gd.headline2, gd.headline3);

	gd.x_auto = TRUE;
	gd.y_auto = TRUE;

    if(!(gd.outlog=fopen(logfile,"w"))) {
        fprintf(stdout,"error opening file '%s' \n",logfile);
        exit(0);
    }

    cmd.paramfile = GetParam("paramfile");
    if (!strnull(cmd.paramfile))
		StartRunParameterfile();
	else
		StartRunCmdline();
}

local void StartRunParameterfile(void)
{
	ReadParameterFile(cmd.paramfile);
	startrun_ParamStat();
	startrun_Common();
	PrintParameterFile(cmd.paramfile);
}

#define parameter_null	"parameters_null-nxls"

local void StartRunCmdline(void)
{
	ReadParametersCmdline();
	startrun_Common();
	PrintParameterFile(parameter_null);
}

local void ReadParametersCmdline(void)
{
    cmd.inputfile			= GetParam("inputfile");
    cmd.outfile             = GetParam("out");
    cmd.outfilefmt          = GetParam("outfmt");

	cmd.usingcolumns		= GetParam("usingcolumns");
    cmd.operation           = GetParam("operation");
	cmd.factor = GetdParam("factor");

	cmd.xrange				= GetParam("xrange");
	cmd.yrange				= GetParam("yrange");

	cmd.options = GetParam("options");

}

local void startrun_Common(void)
{
	char *pch;
	int i, npcols, nitems;
	short flag;
	char *pusingcolumns[30], *framestyleitems[30];
	char inputfiletmp[200], legendstmp[100],
		usingcolumnstmp[100], framestyletmp[50];
	int xrangeflag=1, yrangeflag=1;

	if (strnull(cmd.inputfile)) {
		fprintf(stdout,"\nstartrun_Common: no inputfile was given making data ...\n");
		gd.nfiles=1;							// To test data...
	} else {
		strcpy(inputfiletmp,cmd.inputfile);
		fprintf(stdout,"\nSplitting string \"%s\" in tokens:\n",inputfiletmp);
		gd.nfiles=0;
		pch = strtok(inputfiletmp," ,");
		while (pch != NULL) {
			gd.filenames[gd.nfiles] = (string) malloc(100);
			strcpy(gd.filenames[gd.nfiles],pch);
			++gd.nfiles;
			fprintf(stdout,"%s\n",gd.filenames[gd.nfiles-1]);
			pch = strtok (NULL, " ,");
		}
		fprintf(stdout,"num. of files in inputfile %s =%d\n",cmd.inputfile,gd.nfiles);
	}


	if (strcmp(cmd.xrange,"auto"))
		gd.x_auto=FALSE;

	if (!(sscanf(cmd.xrange, "%lf:%lf", &gd.xmin, &gd.xmax) == 2))
		xrangeflag=0;
	if ( !gd.x_auto && xrangeflag==0)
		error("\nStartRunParameterfile: xrange must be auto or in the form xmin:xmax\n\n");
	if (xrangeflag != 0 && gd.xmin >= gd.xmax)
		error("\nStartRunParameterfile: xmin must be < xmax : (xmin,xmax)=(%lf,%lf)\n\n",gd.xmin,gd.xmax);

	if (strcmp(cmd.yrange,"auto"))
		gd.y_auto=FALSE;
	if (!(sscanf(cmd.yrange, "%lf:%lf", &gd.ymin, &gd.ymax) == 2))
		yrangeflag=0;
	if ( !gd.y_auto && yrangeflag==0)
		error("\nStartRunParameterfile: yrange must be auto or in the form ymin:ymax\n\n");
	if (yrangeflag != 0 && gd.ymin >= gd.ymax)
		error("\nStartRunParameterfile: ymin must be < ymax : (ymin,ymax)=(%lf,%lf)\n\n",gd.ymin,gd.ymax);

	strcpy(usingcolumnstmp,cmd.usingcolumns);
	fprintf(stdout,"\nSplitting string \"%s\" in tokens:\n",usingcolumnstmp);
	npcols=0;
	pch = strtok(usingcolumnstmp," ,");
	while (pch != NULL) {
		pusingcolumns[npcols] = (string) malloc(10);
		strcpy(pusingcolumns[npcols],pch);
		++npcols;
		fprintf(stdout,"%s\n",pusingcolumns[npcols-1]);
		pch = strtok (NULL, " ,");
	}
	fprintf(stdout,
		"num. of pairs of colmuns in usingcolumns %s =%d\n",cmd.usingcolumns,npcols);

	if (npcols != gd.nfiles)
		error("\nStartRunParameterfile: nusingcolumns must be equal to number of files\n\n");

	gd.vcol1 = (int *) allocate(npcols*sizeof(int));
	gd.vcol2 = (int *) allocate(npcols*sizeof(int));
    gd.vcol3 = (int *) allocate(npcols*sizeof(int));

    printf("\n Aqui voy (1) \n");
	for (i=0; i<npcols; i++) {
				if (!(sscanf(pusingcolumns[i], "%d:%d:%d",
					&gd.vcol1[i], &gd.vcol2[i], &gd.vcol3[i]) == 3))
					error("\nStartRunCmdline: usingcolumns must be in the form c1:c2:c3\n\n");
				fprintf(stdout,"pairs: %d %d %d\n",gd.vcol1[i],gd.vcol2[i],gd.vcol3[i]);
	}

    printf("\n Aqui voy (2) \n");

	operation_string_to_int(cmd.operation, &gd.operation_int);
	PrintParametersLog();
}

local void startrun_ParamStat(void)
{
	if (GetParamStat("out") & ARGPARAM)
		cmd.outfile = GetParam("out");
	if (GetParamStat("outfmt") & ARGPARAM)
		cmd.outfilefmt = GetParam("outfmt");

	if (GetParamStat("operation") & ARGPARAM)
		cmd.operation = GetParam("operation");

	if (GetParamStat("options") & ARGPARAM)
		cmd.options = GetParam("options");
}

local void CheckParameters(void)
{
}


local void PrintParametersLog(void)
{
	fprintf(gd.outlog,"\nParameters:\n");
	fprintf(gd.outlog,"x_auto= %s",gd.x_auto ? "true" : "false");
	fprintf(gd.outlog,"y_auto= %s",gd.y_auto ? "true" : "false");
	fprintf(gd.outlog,"\nxmin= %g",gd.xmin);
	fprintf(gd.outlog,"\nxmax= %g",gd.xmax);
	fprintf(gd.outlog,"\nymin= %g",gd.ymin);
	fprintf(gd.outlog,"\nymax= %g",gd.ymax);
	fflush(gd.outlog);
}

#undef logfile
#undef parameter_null

local void ReadParameterFile(char *fname)
{
#define DOUBLE 1
#define STRING 2
#define INT 3
#define BOOLEAN 4
#define MAXTAGS 300

  FILE *fd,*fdout;

  char buf[200],buf1[200],buf2[200],buf3[200];
  int  i,j,nt;
  int  id[MAXTAGS];
  void *addr[MAXTAGS];
  char tag[MAXTAGS][50];
  int  errorFlag=0;

  nt=0;

	SPName(cmd.inputfile,"inputfile",200);
	SPName(cmd.outfile,"out",100);
	SPName(cmd.outfilefmt,"outfmt",100);
	SPName(cmd.usingcolumns,"usingcolumns",100);
	SPName(cmd.operation,"operation",100);
	RPName(cmd.factor,"factor");
	SPName(cmd.xrange,"xrange",1);
	SPName(cmd.yrange,"yrange",1);
	SPName(cmd.options,"options",100);

//------------------------------------------------------------------------------

    if((fd=fopen(fname,"r"))) {
        while(!feof(fd)) {
            fgets(buf,200,fd);
            if(sscanf(buf,"%s%s%s",buf1,buf2,buf3)<1)
                continue;
            if(sscanf(buf,"%s%s%s",buf1,buf2,buf3)<2)
                *buf2='\0'; //
//            *buf2=(char)NULL;
            // Removing the warning:
            // warning: cast from pointer to integer of different size [-Wpointer-to-int-cast]
            if(buf1[0]=='%')
		continue;
            for(i=0,j=-1;i<nt;i++)
                if(strcmp(buf1,tag[i])==0) {
                    j=i;
		    tag[i][0]=0;
		    break;
            }
            if(j>=0) {
                switch(id[j]) {
		    case DOUBLE:
		      *((double*)addr[j])=atof(buf2); 
		      break;
		    case STRING:
		      strcpy(addr[j],buf2);
		      break;
		    case INT:
		      *((int*)addr[j])=atoi(buf2);
		      break;
		    case BOOLEAN:
                        if (strchr("tTyY1", *buf2) != NULL) {          
                            *((bool*)addr[j])=TRUE;
                        } else 
                            if (strchr("fFnN0", *buf2) != NULL)  {
                                *((bool*)addr[j])=FALSE;
                            } else {
                                error("GetbParam: %s=%s not bool\n", buf1, buf2);
                            }
		      break;
                }
            } else {
                fprintf(stdout,
                    "Error in file %s:   Tag '%s' not allowed or multiple defined.\n",
                    fname,buf1);
                errorFlag=1;
            }
        }
        fclose(fd);
    } else {
        fprintf(stdout,"Parameter file %s not found.\n", fname);
        errorFlag=1;
        exit(1); 
    }
  
    for(i=0;i<nt;i++) {
        if(*tag[i]) {
            fprintf(stdout,
                "Error. I miss a value for tag '%s' in parameter file '%s'.\n",
                tag[i],fname);
            exit(0);
        }
    }
#undef DOUBLE 
#undef STRING 
#undef INT 
#undef BOOLEAN
#undef MAXTAGS
}

#define FMTT	"%-35s%s\n"
#define FMTI	"%-35s%d\n"
#define FMTR	"%-35s%g\n"

local void PrintParameterFile(char *fname)
{
    FILE *fdout;
    char buf[200];
    
    sprintf(buf,"%s%s",fname,"-usedvalues");
    if(!(fdout=fopen(buf,"w"))) {
        fprintf(stdout,"error opening file '%s' \n",buf);
        exit(0);
    } else {
        fprintf(fdout,"%s\n",
            "%-------------------------------------------------------------------");
        fprintf(fdout,"%s %s\n","% Parameter input file for:",gd.headline0);
        fprintf(fdout,"%s\n","%");
        fprintf(fdout,"%s %s: %s\n%s\t    %s\n","%",gd.headline1,gd.headline2,"%",
            gd.headline3);
        fprintf(fdout,"%s\n%s\n",
            "%-------------------------------------------------------------------",
            "%");

        fprintf(fdout,FMTT,"inputfile",cmd.inputfile);
        fprintf(fdout,FMTT,"out",cmd.outfile);
        fprintf(fdout,FMTT,"outfmt",cmd.outfilefmt);
        fprintf(fdout,FMTT,"operation",cmd.operation);
        fprintf(fdout,FMTR,"factor",cmd.factor);
        fprintf(fdout,FMTT,"usingcolumns",cmd.usingcolumns);
        fprintf(fdout,FMTT,"xrange",cmd.xrange);
        fprintf(fdout,FMTT,"yrange",cmd.yrange);
        fprintf(fdout,FMTT,"options",cmd.options);
        fprintf(fdout,"\n\n");					// to read as input paramfile

    }
    fclose(fdout);
}

#undef FMTT
#undef FMTI
#undef FMTR


local void scanbOption(string optionstr, bool *option, int *noption, 
	int nfiles, int flag, string message)
{
	char *pch;
	char *poptionstr[30],  optiontmp[100];
	int i;

	fprintf(stdout,"\nProcessing '%s' option:\n", message);

	if (!strnull(optionstr)) {
		strcpy(optiontmp,optionstr);
		fprintf(stdout,"\nSplitting string \"%s\" in tokens:\n",optiontmp);
		*noption=0;
		pch = strtok(optiontmp," ,");
		while (pch != NULL) {
			poptionstr[*noption] = (string) malloc(10);
			strcpy(poptionstr[*noption],pch);
			++(*noption);
			fprintf(stdout,"%s\n",poptionstr[*noption-1]);
			pch = strtok (NULL, " ,");
		}
		fprintf(stdout,"num. of tokens in option %s =%d\n",
			optionstr,*noption);

		if (flag == 0)
			if (*noption != nfiles)
				error("\nscanOption: noption = %d must be equal to number of files\n\n",*noption);
		if (*noption > MAXLINES)
			error("\nscanOption: noption = %d must be less than the maximum num. of lines\n\n",*noption);

		for (i=0; i<*noption; i++) {
			if (strchr("tTyY1", *poptionstr[i]) != NULL) {
				option[i]=TRUE;
			} else {
				if (strchr("fFnN0", *poptionstr[i]) != NULL)
					option[i]=FALSE;
				else 
					error("\nscanOption: not bool in %s",poptionstr[i]);
			}
			fprintf(stdout,"option: %s\n", option[i] ? "true" : "false");
		}

		fprintf(stdout,"\nnoptions, nfiles: %d %d\n",*noption,nfiles);
		if (flag == 1) {
			if (*noption > nfiles)
				error("\nscanOption: noption = %d must be less or equal to number of files\n\n",*noption);
			else {
				for (i=*noption; i<nfiles; i++) {
					option[i]=option[0];
					fprintf(stdout,"option: %s\n", option[i] ? "true" : "false");
				}
				for (i=*noption; i<nfiles; i++) {
					option[i]=option[0];
					fprintf(stdout,"option: %d\n",option[i]);
				}
			}
		}
	} else {
		for (i=0; i<nfiles; i++) {
			option[i]=FALSE;
			fprintf(stdout,"option: %s\n", option[i] ? "true" : "false");
		}
	}
}


local void scaniOption(string optionstr, int *option, int *noption, 
	int nfiles, int flag, string message)
{
	char *pch;
	char *poptionstr[30],  optiontmp[100];
	int i;

	fprintf(stdout,"\nProcessing '%s' option:\n", message);

	if (!strnull(optionstr)) {
		strcpy(optiontmp,optionstr);
		fprintf(stdout,"\nSplitting string \"%s\" in tokens:\n",optiontmp);
		*noption=0;
		pch = strtok(optiontmp," ,");
		while (pch != NULL) {
			poptionstr[*noption] = (string) malloc(10);
			strcpy(poptionstr[*noption],pch);
			++(*noption);
			fprintf(stdout,"%s\n",poptionstr[*noption-1]);
			pch = strtok (NULL, " ,");
		}
		fprintf(stdout,"num. of tokens in option %s =%d\n",
			optionstr,*noption);

		if (flag == 0)
			if (*noption != nfiles)
				error("\nscanOption: noption = %d must be equal to number of files\n\n",*noption);
		if (*noption > MAXLINES)
			error("\nscanOption: noption = %d must be less than the maximum num. of lines\n\n",*noption);

		for (i=0; i<*noption; i++) {
			option[i]=atoi(poptionstr[i]);
			fprintf(stdout,"option: %d\n",option[i]);
		}
		fprintf(stdout,"\nnoptions, nfiles: %d %d\n",*noption,nfiles);
		if (flag == 1) {
			if (*noption > nfiles)
				error("\nscanOption: noption = %d must be less or equal to number of files\n\n",*noption);
			else {
				for (i=*noption; i<nfiles; i++) {
					option[i]=option[i-1]+1;
					fprintf(stdout,"option: %d\n",option[i]);
				}
			}
		}
	} else {
		for (i=0; i<nfiles; i++) {
			option[i]=1;
			fprintf(stdout,"option: %d\n",option[i]);
		}
	}
}

