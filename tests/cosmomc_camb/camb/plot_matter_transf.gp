set term post eps color enhanced 18
set output "plot_matter_transf.eps"

set key top right

set title "Matter power spectrum and transfer function"
set xlabel "k" 
set ylabel "Spectrum"

set logscale x
set logscale y

p 'test_matterpower.dat' u 1:2 t 'Model Power Spectrum' w l lw 3, \
'test_transfer_out.dat' u 1:2 t 'Model Transfer Function' w l lw 3
