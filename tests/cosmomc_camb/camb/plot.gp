set term post eps color enhanced 18
set output "plot.eps"

set key top right

set title "CMB anisotropies"
set xlabel "Multipole l" 
set ylabel "l(l+1)C_l ({/Symbol m}K)^2"

set logscale x

p 'test_scalCls.dat' u 1:2 t 'Model' w l lw 3, \
'wmap_binned_tt_powspec_3yr_v2.txt' \
u 1:4:5 t 'WMAP3y' w errorbars
